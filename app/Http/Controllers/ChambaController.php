<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Illuminate\Mail\Mailable;
use Swift_Mailer;
use App\Mail\MailActividad;
use \Swift_SmtpTransport as SmtpTransport;
use DB;
use Illuminate\Support\Facades\Session;

class ChambaController extends Controller
{
    protected $path = 'images/'; //path para pruebas locales
    public function home()
    {

        $data['fotos'] =   DB::table('gal_inicio')->get();
        return view('home')->with( $data);
    }

    public function edit_inicio()
    {

        $data['fotos'] =   DB::table('gal_inicio')->get();
        return view('edit_inicio')->with( $data);
    }

    public function edit_chamba()
    {

        $data['fotos'] =   DB::table('gal_chamba')->get();
        return view('edit_chamba')->with( $data);
    }


    public function index()
    {
        $data['fotos'] =   DB::table('gal_chamba')->get();

        return view('/chamba')->with( $data);

    }

    public function que()
    {
        $data['fotos'] =   DB::table('foto_que')->first();
        return view('/que')->with( $data);

    }

    public function edit_que()
    {
        $data['fotos'] =   DB::table('foto_que')->first();
        return view('/edit_que')->with( $data);

    }


    public function blog()
    {
        $data['blogs'] =   DB::table('blog')->get();
        return view('/blog')->with( $data);

    }

    public function delBlog(Request $request)
    {
        //borramos el blog
        DB::table('blog')->where('id', '=', $request['id'])->delete();
        DB::table('blog_com')->where('blog_id', '=', $request['id'])->delete();


        return response()->json(['message' => "OK"] );
    }

    public function addBlog()
    {

        return view('/add_blog');

    }


    public function blog_admin()
    {
        $data['blogs'] =   DB::table('blog')->get();
        return view('/blog_admin')->with( $data);

    }

    public function regFotoBlog(Request $request)
    {
        $file = $request->file('photo');

        $nombre = "_foto_blog";
        $extension = $file->getClientOriginalExtension();
        $random = str_random(10);
        $fecha = date('Ymdhis');
        $nombre = $random."_".$fecha."-".$nombre.".".$extension;
        $file->move($this->path.'/', $nombre);
        $ruta = $this->path.'/'.$nombre;


        return response()->json(['message' =>  $nombre, 'path' =>  $ruta]);
    }

    public function regFotoIni(Request $request)
    {
        $file = $request->file('photo');
        //buscamos el nombre del archivo
        $foto =   DB::table('gal_inicio')
            ->where('gal_inicio.id','=',$request['id'])
            ->first();

        $nombre = "_Archivo_Inicio";
        $extension = $file->getClientOriginalExtension();
        $random = str_random(10);
        $fecha = date('Ymdhis');
        $nombre = $random."_".$fecha."-".$nombre.".".$extension;

       // $rand_foto_name = date('dmYhis');

       // $nombre = $rand_foto_name."_".$foto->foto;
        $file->move($this->path.'/jey/', $nombre);
        $ruta = $this->path.'/jey/'.$nombre;

        //actualizamos la ruta en base de datos
        DB::table('gal_inicio')
            ->where('id', $request['id'])
            ->update([
                'foto' =>  $nombre

            ]);


        return response()->json(['message' =>  $nombre, 'path' =>  $ruta]);
    }

    public function regFotoChamba(Request $request)
    {
        $file = $request->file('photo');
        //buscamos el nombre del archivo
        $foto =   DB::table('gal_chamba')
            ->where('gal_chamba.id','=',$request['id'])
            ->first();

        $nombre = "_Archivo_Chamba";
        $extension = $file->getClientOriginalExtension();
        $random = str_random(10);
        $fecha = date('Ymdhis');
        $nombre = $random."_".$fecha."-".$nombre.".".$extension;
        $file->move($this->path.'/', $nombre);
        $ruta = $this->path.'/'.$nombre;

        //actualizamos la ruta en base de datos
        DB::table('gal_chamba')
            ->where('id', $request['id'])
            ->update([
                'foto' =>  $nombre

            ]);


        return response()->json(['message' =>  $nombre, 'path' =>  $ruta]);
    }

    public function regFotoTodo(Request $request)
    {
        $file = $request->file('photo');
        //buscamos el nombre del archivo
        $foto =   DB::table('gal_todo')
            ->where('gal_todo.id','=',$request['id'])
            ->first();
        $nombre = "_Archivo_Todo";
        $extension = $file->getClientOriginalExtension();
        $random = str_random(10);
        $fecha = date('Ymdhis');
        $nombre = $random."_".$fecha."-".$nombre.".".$extension;
        $file->move($this->path.'/', $nombre);
        $ruta = $this->path.'/'.$nombre;

        //actualizamos la ruta en base de datos
        DB::table('gal_todo')
            ->where('id', $request['id'])
            ->update([
                'foto' =>  $nombre

            ]);

        return response()->json(['message' =>  $nombre, 'path' =>  $ruta]);
    }

    public function regFotoQue(Request $request)
    {
        $file = $request->file('photo');
        //buscamos el nombre del archivo
        $foto =   DB::table('foto_que')
            ->where('foto_que.id','=',$request['id'])
            ->first();
        $nombre = "_Archivo_Que";
        $extension = $file->getClientOriginalExtension();
        $random = str_random(10);
        $fecha = date('Ymdhis');
        $nombre = $random."_".$fecha."-".$nombre.".".$extension;
        $file->move($this->path.'/', $nombre);
        $ruta = $this->path.'/'.$nombre;
        //actualizamos la ruta en base de datos
        DB::table('foto_que')
            ->where('id', $request['id'])
            ->update([
                'foto' =>  $nombre

            ]);

        return response()->json(['message' =>  $nombre, 'path' =>  $ruta]);
    }

    public function insBlog(Request $request)
    {
        DB::table('blog')->insert([
            [
                'titulo' =>  $request['titulo'],
                'corta' =>  $request['corta'],
                'larga' =>  $request['larga'],
                'imagen' =>  $request['imagen'],
                'youtube' =>  $request['youtube'],
                'fecha' =>  date('Y-m-d'),
                'coments' =>0

            ]
        ]);

        return response()->json(['message' =>  $request->all()]);
    }

    public function editBlog($id)
    {
        $data['contenido'] =   DB::table('blog')
            ->where('blog.id','=',$id)
            ->first();


        return view('/edit_blog')->with( $data);;


    }

    public function updBlog(Request $request)
    {
        DB::table('blog')
            ->where('id', $request['id'])
            ->update([
                'titulo' =>  $request['titulo'],
                'corta' =>  $request['corta'],
                'larga' =>  $request['larga'],
                'imagen' =>  $request['imagen'],
                'youtube' =>  $request['youtube'],
            ]);

        return response()->json(['message' =>  $request->all()]);

    }

    public function todo()
    {

        $data['fotos'] =   DB::table('gal_todo')->get();

        return view('/todo')->with( $data);

    }
    public function edit_todo()
    {

        $data['fotos'] =   DB::table('gal_todo')->get();

        return view('/edit_todo')->with( $data);

    }

    public function contacto()
    {

        return view('/contacto');

    }

    public function mail(Request $request)
    {

        // Setup a new SmtpTransport instance for Gmail
        $transport = new SmtpTransport();
        $transport->setHost('mail.lajey.com');
        $transport->setPort(587);
        $transport->setEncryption('tls');
        $transport->setUsername('info@lajey.com');
        $transport->setPassword('*,1t&7w3j9eo');


        // Assign a new SmtpTransport to SwiftMailer
        $driver = new Swift_Mailer($transport);

        // Assign it to the Laravel Mailer
        Mail::setSwiftMailer($driver);

        $data['nombre'] = $request['nombre'];
        $data['mail'] = $request['mail'];
        $data['page'] = $request['page'];
        $data['msj'] = $request['msj'];

        // Send your message
        Mail::to('quiubo@lajey.com')->send(new MailActividad($data));
        return response()->json(['message' => "OK"] );

    }

    public function coment(Request $request)
    {

        $data['nombre'] = $request['nombre'];
        $data['coments'] = $request['coments'];
        $data['id'] = $request['id'];
        $data['msj'] = $request['msj'];
        $coments = $data['coments'] + 1;

        DB::table('blog_com')->insert([
            [
                'blog_id' =>  $data['id'],
                'comentario' =>  $data['msj'],
                'autor' =>  $data['nombre'],
                'fecha' =>  date('Y-m-d')

            ]
        ]);

        DB::table('blog')
            ->where('id', $request['id'])
            ->update([
                'coments' =>  $coments,

            ]);

        return response()->json(['message' => "OK"] );

    }

    public function contenido($id)
    {
        $data['contenido'] =   DB::table('blog')
            ->where('blog.id','=',$id)
            ->first();

        $data['comentarios'] =   DB::table('blog_com')
            ->where('blog_com.blog_id','=',$id)
            ->get();
        return view('/content')->with( $data);

    }

    public function getSignOut() {

        Session::flush();
        $data['fotos'] =   DB::table('gal_inicio')->get();
        return view('home')->with( $data);
    }
}
