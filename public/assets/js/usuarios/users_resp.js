
$('#tipoUsuario').on('change' , function () {
    //alert(document.getElementsByName('tipo')[0].value);
    $.ajax({
        method: 'POST',
        url: url,
        data: {tipo:document.getElementsByName('tipo')[0].value, cliente: 0, _token: token}
    })
        .done(function(msg){
            console.log(msg['message'])
            fillDataTable(msg)

        });
});

$('#clienteId').on('change' , function () {
    //alert(document.getElementsByName('tipo')[0].value);
    $.ajax({
        method: 'POST',
        url: url,
        data: {tipo:0, cliente: document.getElementsByName('cliente')[0].value, _token: token}
    })
        .done(function(msg){

            console.log(msg['message'])
            fillDataTable(msg);

        });
});


/**filtro por tipo de usuario**/
$('#addUser').on('click' , function () {

    if($('input[name=tipo_usuario]:checked').val() == 1)
    {
        add();
    }else
    {
        if($('#selCliente option:selected').val() == 0)
        {
            $('.ajax-content').html(' <p class="alert alert-danger" >' + Lang.get('users_js.new_validation_cliente_error') + '</p>')
        }else
        {
            $('.ajax-content').html('');
            add();
        }
    }
});
/**agregar usuario**/
function add()
{
    var data = $("#formAddUser").serializeFormJSON();
    $.ajax({
        method: 'POST',
        url: url_add,
        data: {body: data, _token: token}
    }).done(function(msg){

        console.log(msg['message']);
        if(msg['status'] == 'fail')
        {
            var errores = '';
            errores +=    (msg['message']['nombre']  ?  ' <p class="alert alert-danger" >'+msg['message']['nombre']+'</p>' : '');
            errores +=    (msg['message']['acc_mail']  ?   ' <p class="alert alert-danger" >'+msg['message']['acc_mail']+'</p>' : '');
            errores +=    (msg['message']['password']  ?   ' <p class="alert alert-danger" >'+msg['message']['password']+'</p>' : '');
            $('.ajax-content').html(errores)
        }else
        {
            var errores = ' ';
            $('.ajax-content').html(errores)
            $('#exampleModalLong').modal('toggle');
            fillDataTable(msg);
        }
    }).fail(function(msg){

        console.log(msg['message']);

    });
}
/**transformar información del post en JSON**/
(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);


$('#optionsRadios2').on('click' , function () {
    $('#selCliente').attr("disabled", false);
});

$('#optionsRadios1').on('click' , function () {
    $('#selCliente').attr("disabled", true);
});

$('#optionsRadios2Edit').on('click' , function () {
    $('#selClienteEdit').attr("disabled", false);
});

$('#optionsRadios1Edit').on('click' , function () {
    $('#selClienteEdit').attr("disabled", true);
});


/**llenado y configuracion del data table**/
function fillDataTable(msg)
{
    $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

        }
    } );
    var table = "";
    var dataSet = [];
    for(var i = 0; i < msg['message'].length; i++)
    {
        var name_status = (msg['message'][i]['estatus'] == 1 ? Lang.get('users_js.table_field_status_active') : Lang.get('users_js.table_field_status_inactive'));
        var tipo_usuario = '';
        switch(msg['message'][i]['tipo_usuario'])
        {
            case "1":
                tipo_usuario = Lang.get('users_js.table_field_usertype_admin_desc');
                break;
            case "2":
                tipo_usuario = Lang.get('users_js.table_field_usertype_custom_desc');
                break;
            case "3":
                tipo_usuario = Lang.get('users_js.table_field_usertype_record_desc');
                break;
        }
        dataSet.push([ msg['message'][i]['id'],
            msg['message'][i]['nombre'],
            msg['message'][i]['acc_mail'],
            name_status,
            tipo_usuario,
            msg['message'][i]['secu_mail'],
            msg['message'][i]['cliente'],
            msg['message'][i]['aud_add_user'],
            msg['message'][i]['aud_add_fh'],
            msg['message'][i]['aud_upd_user'],
            msg['message'][i]['aud_upd_fh'],
            msg['message'][i]['clientes_id'],
            msg['message'][i]['tipo_usuario'],
            msg['message'][i]['estatus'],
        ] );
    }
    table = $('#example').DataTable( {

        "responsive": true,
        destroy: true,
        "processing": true,
        "language": {
            "lengthMenu": Lang.get('tables_js.table_config_language.lengthMenu'),
            "zeroRecords": Lang.get('tables_js.table_config_language.zeroRecords'),
            "info": Lang.get('tables_js.table_config_language.info'),
            "infoEmpty": Lang.get('tables_js.table_config_language.infoEmpty'),
            "infoFiltered": Lang.get('tables_js.table_config_language.infoFiltered'),
            "search": Lang.get('tables_js.table_config_language.search'),
            "paginate": {
                "first":      Lang.get('tables_js.table_config_language.paginate.first'),
                "last":       Lang.get('tables_js.table_config_language.paginate.last'),
                "next":       Lang.get('tables_js.table_config_language.paginate.next'),
                "previous":   Lang.get('tables_js.table_config_language.paginate.previous')
            }
        },
        data: dataSet,
        columns: [
            { title: Lang.get('users_js.table_columns.id') },
            { title: Lang.get('users_js.table_columns.nombre') },
            { title: Lang.get('users_js.table_columns.mail') },
            { title: Lang.get('users_js.table_columns.estatus') },
            { title: Lang.get('users_js.table_columns.tipo') },
            { title: Lang.get('users_js.table_columns.secu_mail') },
            { title: Lang.get('users_js.table_columns.cliente') },
            { title: Lang.get('users_js.table_columns.aud_user') },
            { title: Lang.get('users_js.table_columns.aud_add_fh') },
            { title: Lang.get('users_js.table_columns.aud_upd') },
            { title: Lang.get('users_js.table_columns.aud_upd_fh') },
            { title: Lang.get('users_js.table_columns.clientes_id') },
            { title: Lang.get('users_js.table_columns.tipo_usuario') }
        ],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 5 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 6 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 7 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 8 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 9 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 10 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 11 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 12 ],
                "visible": false,
                "searchable": false
            },
        ]
    } );
    /**poder seleccionar una fila**/
    $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

        }
    } );
    /**ver usuario**/
    $('#btnVer').on('click' , function () {
        var mystr = "";
        if(table.row('.selected').data())
        {

            //Loading the variable
            var mystr = table.row('.selected').data()+ ' ';

            //Splitting it with : as the separator
            var myarr = mystr.split(",");
            //Then read the values from the array where 0 is the first
            var myvar = myarr[0] + ":" + myarr[1];
            //alert(table.row('.selected').data());
            $('.ajax-nombre').html( myarr[1])
            $('.ajax-mail').html( myarr[2])
            $('.ajax-status').html( myarr[3])
            $('.ajax-tipo').html( myarr[4])
            $('.ajax-secumail').html( myarr[5])
            $('.ajax-cliente').html( myarr[6])
            //$('.ajax-ins').html( 'Usuario: '+myarr[7]+'</BR> Fecha: '+ myarr[8])
            //$('.ajax-upd').html( 'Usuario: '+myarr[9]+'</BR> Fecha: '+ myarr[10])
            getAuds(get_auds, token, myarr[7], myarr[8], 'ins')
            getAuds(get_auds, token, myarr[9], myarr[10], 'upd')
            $('#ModalView').modal();
        }else
        {
            bootbox.alert(Lang.get('functions.alert_selection'));
        }


    });
    /**editar usuario**/
    $('#btnEdit').on('click' , function () {
        var mystr = "";
        if(table.row('.selected').data())
        {
            var select = "";
            //Loading the variable
            var mystr = table.row('.selected').data()+ ' ';
            //Splitting it with : as the separator
            var myarr = mystr.split(",");
            //Then read the values from the array where 0 is the first
            var myvar = myarr[0] + ":" + myarr[1];
            //alert(table.row('.selected').data());
            $('.ajax-id').html( '<input type="hidden" class="form-control" name="id" value="'+myarr[0]+'"/>')
            $('.ajax-nombre').html( '<input type="text" class="form-control"  name="nombre" aria-describedby="emailHelp"\n' +
                ' value="'+myarr[1]+'" required="true">')
            $('.ajax-mail').html(' <input type="email" class="form-control"  name="acc_mail" aria-describedby="emailHelp"\n' +
                '                                   value="'+myarr[2]+'">');
            $('.ajax-mailsecu').html(' <input type="email" class="form-control"  name="secu_mail" aria-describedby="emailHelp"\n' +
                '                                   value="'+myarr[5]+'">');
            if(myarr[12] == 1)
            {

                $('#selection').replaceWith( ' <option value="0" selected>' + Lang.get('users_js.selection_noapply') + '</option>');
                $('#selClienteEdit').attr("disabled", true);
                $("#optionsRadios1Edit").attr("checked", true);
                $("#optionsRadios2Edit").attr("checked", false);

            }else
            {

                $('#selection').replaceWith(' <option value="'+myarr[11]+'" selected>'+myarr[6]+'</option>');
                $('#selClienteEdit').attr("disabled", false);
                $("#optionsRadios1Edit").attr("checked", false);
                $("#optionsRadios2Edit").attr("checked", true);
            }

            $('.ajax-cliente').html(select);
            $('#ModalEdit').modal();
        }else
        {
            bootbox.alert(Lang.get('functions.alert_selection'));
        }

    });
    /**alerta de activar usuario**/
    $('#btnAct').on('click' , function () {
        var mystr = "";
       if(table.row('.selected').data())
       {
           //Loading the variable
           var mystr = table.row('.selected').data()+ ' ';
           //Splitting it with : as the separator
           var myarr = mystr.split(",");
           //Then read the values from the array where 0 is the first
           var myvar = myarr[0] + ":" + myarr[1];
           var name_status = (myarr[13] == 1 ? Lang.get('users_js.question_activate_desc') : Lang.get('users_js.question_inactivate_desc '));
           var val_status = (myarr[13] == 1 ? 0 : 1);
           //alert(table.row('.selected').data());
           $('.ajax-idact').html( '<input type="hidden" class="form-control" name="id" value="'+myarr[0]+'"/>')
           $('.ajax-estatus').html( '<input type="hidden" class="form-control" name="estatus" value="'+val_status+'"/>')
           $('.ajax-pregunta').html(' <h4 >' + Lang.get('users_js.question_activate', { 'estatus': name_status }) + '</h4>');
           $('.ajax-titulo').html(' <h5 class="modal-title" id="ModalLongTitle">'+name_status+' '+Lang.get('users_js.title_head_modal')+'</h5>');
           $('#ModalActivate').modal();
       }else
       {
           bootbox.alert(Lang.get('functions.alert_selection'));
       }

    });
    /**proceso de activar/desactivar usuario**/
    $('#actUser').on('click' , function () {
        var data = $("#formActUser").serializeFormJSON();
        $.ajax({
            method: 'POST',
            url: url_activate,
            data: {body: data, _token: token}
        }).done(function(msg){

            console.log(msg['message']);
            if(msg['status'] == 'fail')
            {
                console.log(msg['message']);
            }else
            {
                $('#ModalActivate').modal('toggle');
                fillDataTable(msg);
            }
        }).fail(function(msg){

            console.log(msg['message']);

        });
    });
    /**filtro por tipo de usuario**/
    $('#updUser').on('click' , function () {

        if($('input[name=tipo_usuario]:checked').val() == 1)
        {
            edit();
        }else
        {
            if($('#selClienteEdit option:selected').val() == 0)
            {
                $('.ajax-content').html(' <p class="alert alert-danger" >' + Lang.get('users_js.upd_validation_cliente_error') + '</p>')
            }else
            {
                $('.ajax-content').html('');
                edit();
            }
        }
    });

    /**agregar usuario**/
    function edit()
    {
        var data = $("#formUpdUser").serializeFormJSON();
        $.ajax({
            method: 'POST',
            url: url_edit,
            data: {body: data, _token: token}
        }).done(function(msg){

            console.log(msg['message']);
            if(msg['status'] == 'fail')
            {
                var errores = '';
                errores +=    (msg['message']['nombre']  ?  ' <p class="alert alert-danger" >'+msg['message']['nombre']+'</p>' : '');
                errores +=    (msg['message']['acc_mail']  ?   ' <p class="alert alert-danger" >'+msg['message']['acc_mail']+'</p>' : '');
                errores +=    (msg['message']['password']  ?   ' <p class="alert alert-danger" >'+msg['message']['password']+'</p>' : '');
                $('.ajax-error').html(errores)
            }else
            {
                var errores = ' ';
                $('.ajax-error').html(errores)
                $('#ModalEdit').modal('toggle');
                fillDataTable(msg);
            }
        }).fail(function(msg){

            console.log(msg['message']);

        });
    }


}

