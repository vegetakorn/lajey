
$('#tipoUsuario').on('change' , function () {
    //alert(document.getElementsByName('tipo')[0].value);
    $.ajax({
        method: 'POST',
        url: url,
        data: {tipo:document.getElementsByName('tipo')[0].value, cliente: 0, _token: token}
    })
        .done(function(msg){
            console.log(msg['message'])
            fillDataTable(msg)

        });
});

$('#clienteId').on('change' , function () {
    //alert(document.getElementsByName('tipo')[0].value);
    $.ajax({
        method: 'POST',
        url: url,
        data: {tipo:0, cliente: document.getElementsByName('cliente')[0].value, _token: token}
    })
        .done(function(msg){

            console.log(msg['message'])
            fillDataTable(msg);

        });
});


/**transformar información del post en JSON**/
(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);


$('#optionsRadios2').on('click' , function () {
    $('#selCliente').attr("disabled", false);
});

$('#optionsRadios1').on('click' , function () {
    $('#selCliente').attr("disabled", true);
});

$('#optionsRadios2Edit').on('click' , function () {
    $('#selClienteEdit').attr("disabled", false);
});

$('#optionsRadios1Edit').on('click' , function () {
    $('#selClienteEdit').attr("disabled", true);
});



/**llenado y configuracion del data table**/
function fillDataTable(msg)
{
    $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

        }
    } );
    var table = "";
    var dataSet = [];
    console.log(msg['message']);
    for(var i = 0; i < msg['message'].length; i++)
    {
        var name_status = (msg['message'][i]['estatus'] == 1 ? Lang.get('users_js.table.fields.status.active') : Lang.get('users_js.table.fields.status.inactive'));
        var tipo_usuario = '';
        switch(msg['message'][i]['tipo_usuario'])
        {
            case "1":
                tipo_usuario = Lang.get('users_js.table.fields.usertype.admin.desc');
                break;
            case "2":
                tipo_usuario = Lang.get('users_js.table.fields.usertype.custom.desc');
                break;
            case "3":
                tipo_usuario = Lang.get('users_js.table.fields.usertype.record.desc');
                break;
        }
        dataSet.push({
            'id': msg['message'][i]['id'],
            'nombre': msg['message'][i]['nombre'],
            'acc_mail': msg['message'][i]['acc_mail'],
            'estatus': msg['message'][i]['estatus'],
            'estatus_name': name_status,
            'tipo_usuario': tipo_usuario,
            'cliente': msg['message'][i]['cliente'],
            'cliente_id': msg['message'][i]['clientes_id'],
        });
    }
    table = $('#example').DataTable( {

        "responsive": true,
        destroy: true,
        "processing": true,
        "language": {
            "lengthMenu": Lang.get('tables_js.table_config_language.lengthMenu'),
            "zeroRecords": Lang.get('tables_js.table_config_language.zeroRecords'),
            "info": Lang.get('tables_js.table_config_language.info'),
            "infoEmpty": Lang.get('tables_js.table_config_language.infoEmpty'),
            "infoFiltered": Lang.get('tables_js.table_config_language.infoFiltered'),
            "search": Lang.get('tables_js.table_config_language.search'),
            "paginate": {
                "first":      Lang.get('tables_js.table_config_language.paginate.first'),
                "last":       Lang.get('tables_js.table_config_language.paginate.last'),
                "next":       Lang.get('tables_js.table_config_language.paginate.next'),
                "previous":   Lang.get('tables_js.table_config_language.paginate.previous')
            }
        },
        data: dataSet,
        columns: [
            { data: 'id', title: Lang.get('users_js.table.columns.id') },
            { data: 'nombre', title: Lang.get('users_js.table.columns.nombre') },
            { data: 'acc_mail', title: Lang.get('users_js.table.columns.mail') },
            { data: 'estatus_name', title: Lang.get('users_js.table.columns.estatus') },
            { data: 'tipo_usuario', title: Lang.get('users_js.table.columns.tipo_usuario') },
            {
                data: null, title: Lang.get('functions.tables.columns.actions'),
                sortable: false,
                "render": function (data, type, full, meta) {
                    //Boton para mostrar información
                    botones = '' +
                        '<button data-url="' + url_user_show + '" data-type="show" data-params="' + full.id + '" ' +
                        'type="button" class="btn btn-margin4r btn-secondary btn-fab btn-sm2 btn-load_view" title="' + Lang.get('functions.functions.show') + '" ><i class="icon-account-search"></i></button>';
                    //Botón para editar datos
                    botones = botones +
                        '<button data-url="' + url_user_show + '" data-type="upd" data-params="' + full.id + '" ' +
                        'type="button" class="btn btn-margin4r btn-secondary btn-fab btn-sm2 btn-load_view" title="' + Lang.get('functions.functions.edit') + '" ><i class="icon-pencil"></i></button>';
                    //Boton para activar o inactivar
                    if (full.estatus == 0) {
                        botones = botones +
                            '<button data-url="' + url_changest_view + '" data-type="activar" data-params="' + full.id + '" ' +
                            'type="button" class="btn btn-margin4r btn-secondary btn-fab btn-sm2 btn-load_view" data-toggle="modal" data-target="#modal_general" title="' + Lang.get('functions.functions.activate') + '" ><i class="icon-check"></i></button>';
                    }
                    else {
                        botones = botones +
                            '<button data-url="' + url_changest_view + '" data-type="inactivar" data-params="' + full.id + '" ' +
                            'type="button" class="btn btn-margin4r btn-danger btn-fab btn-sm2 btn-load_view" data-toggle="modal" data-target="#modal_general" title="' + Lang.get('functions.functions.activate') + '" ><i class="icon-close"></i></button>';
                    }
                    return botones;
                }
            },
        ],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
        ]
    } );
    /**poder seleccionar una fila**/
    $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

        }
    } );
}

$(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

});

/**
 * Función que permite cargar vistas Modales de acuerdo a las necesidades
 */
$(document).on("click",".btn-load_view", function(){
    datParametros = $(this).attr("data-params");
    datUrl = $(this).attr("data-url");
    datType = $(this).attr("data-type");
    datToogle = $(this).attr("data-toggle");
    datMethod = $(this).attr("data-method");
    datMethod = (datMethod===undefined)?"POST":datMethod;

    //Obtiene los parámetros necesarios
    dataParams = {};
    switch (datType){
        case "show": //Mostrar client
            dataParams = {id: datParametros, type:'show', _token: token}
            break;
        case "upd": //Actualizar cliente
            dataParams = {id: datParametros, type:'upd', _token: token}
            break;
        case "add": //Agregar cliente
            dataParams = {id: datParametros, type:'add', _token: token}
            break;
        case "activar": //Activar
            dataParams = {id: datParametros, newst:1, _token: token}
            break;
        case "inactivar": //Inactivar
            dataParams = {id: datParametros, newst:0, _token: token}
            break;
        case "messagetmp":
            dataParams = {msg:"Error en módulo", _token: token}
            break;
    }

    $.ajax({
        url: datUrl,
        type: datMethod,
        dataType:"html",
        data: dataParams,
        success:function(data){
            if (datToogle == "modal") {
                $("#modal_general .modal-content").html(data);
            } else {
                $(".page-content").html(data);
            }
        }
    });
});

