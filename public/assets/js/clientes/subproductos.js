/**llenado y configuracion del data table**/
function fillDataTable(msg)
{
    $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
    var table = "";
    var dataSet = [];
    for(var i = 0; i < msg['message'].length; i++)
    {
        //var name_status = (msg['message'][i]['estatus'] == 1 ? 'Activo' : 'Inactivo');
        var name_status = '';

        switch(msg['message'][i]['estatus'])
        {
            case 0:
            case "0":
                name_status = Lang.get('productos_js.table.fields.estatus.0');
                break;
            case 1:
            case "1":
                name_status = Lang.get('productos_js.table.fields.estatus.1');
                break;

        }
        //Si se requieren formatear desde este momento, aquí es el lugar preciso
        dataSet.push({
            "id": msg['message'][i]['id'],
            'clave': msg['message'][i]['clave'],
            'nombre': msg['message'][i]['nombre'],
            'costo': msg['message'][i]['costo'],
            'estatus': msg['message'][i]['estatus'],
            "estatus_name": name_status,
        });
    }
    table = $('#example').DataTable( {

        "responsive": true,
        destroy: true,
        "processing": true,
        "language": {
            "lengthMenu": Lang.get('tables_js.table_config_language.lengthMenu'),
            "zeroRecords": Lang.get('tables_js.table_config_language.zeroRecords'),
            "info": Lang.get('tables_js.table_config_language.info'),
            "infoEmpty": Lang.get('tables_js.table_config_language.infoEmpty'),
            "infoFiltered": Lang.get('tables_js.table_config_language.infoFiltered'),
            "search": Lang.get('tables_js.table_config_language.search'),
            "paginate": {
                "first":      Lang.get('tables_js.table_config_language.paginate.first'),
                "last":       Lang.get('tables_js.table_config_language.paginate.last'),
                "next":       Lang.get('tables_js.table_config_language.paginate.next'),
                "previous":   Lang.get('tables_js.table_config_language.paginate.previous')
            }
        },
        data: dataSet,
        columns: [
            { data: 'id', title: Lang.get('productos_js.subproductos.columns.id') },
            { data: 'clave', title: Lang.get('productos_js.subproductos.columns.clave') },
            { data: 'nombre', title: Lang.get('productos_js.subproductos.columns.nombre') },
            { data: 'costo', title: Lang.get('productos_js.subproductos.columns.costo') },
            { data: 'estatus', title: Lang.get('productos_js.subproductos.columns.estatus') },
            { data: 'estatus_name', title: Lang.get('productos_js.subproductos.columns.estatus') },
            { data: null, title: Lang.get('functions.tables.columns.actions'),
                sortable: false,
                "render": function ( data, type, full, meta ) {
                    //Boton para mostrar información de cliente
                    botones = '' +
                        '<button data-url="' + url_show_subprod + '" data-type="show" data-params="' + full.id + '" ' +
                        'type="button" class="btn btn-margin4r btn-secondary btn-fab btn-sm2 btn-load_view" title="' + Lang.get('functions.functions.show') + '" ><i class="icon-account-search"></i></button>';
                    //Botón para editar datos de cliente
                    botones = botones +
                        '<button data-url="' + url_show_subprod + '" data-type="upd" data-params="' + full.id + '" ' +
                        'type="button" class="btn btn-margin4r btn-secondary btn-fab btn-sm2 btn-load_view" title="' + Lang.get('functions.functions.edit') + '" ><i class="icon-pencil"></i></button>';
                    if (full.estatus == 0 ) {
                        botones = botones +
                            '<button data-url="' + url_ref_changest_view + '" data-type="activar" data-params="' + full.id + '" ' +
                            'type="button" class="btn btn-margin4r btn-secondary btn-fab btn-sm2 btn-load_view" data-toggle="modal" data-target="#modal_general" title="' + Lang.get('functions.functions.activate') + '" ><i class="icon-check"></i></button>';
                    } else if (full.estatus == 1) {
                        botones = botones +
                            '<button data-url="' + url_ref_changest_view + '" data-type="inactivar" data-params="' + full.id + '" ' +
                            'type="button" class="btn btn-margin4r btn-danger btn-fab btn-sm2 btn-load_view" data-toggle="modal" data-target="#modal_general" title="' + Lang.get('functions.functions.referencia') + '" ><i class="icon-close"></i></button>';
                    }
                    return botones;
                }

            }


        ],
        columnDefs: [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 4 ],
                "visible": false,
                "searchable": false
            },
        ]
    } );


    /**transformar información del post en JSON**/
    (function ($) {
        $.fn.serializeFormJSON = function () {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };
    })(jQuery);

}

$(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

});

/**
 * Función que permite cargar vistas Modales de acuerdo a las necesidades
 */
$(document).on("click",".btn-load_view", function(){
    datParametros = $(this).attr("data-params");
    datUrl = $(this).attr("data-url");
    datType = $(this).attr("data-type");
    datToogle = $(this).attr("data-toggle");
    datMethod = $(this).attr("data-method");
    datMethod = (datMethod===undefined)?"POST":datMethod;

    //Obtiene los parámetros necesarios
    dataParams = {};
    switch (datType){
        case "show": //Mostrar client
            dataParams = {id: datParametros, type:'show', _token: token}
            break;
        case "upd": //Actualizar cliente
            dataParams = {id: datParametros, type:'upd', _token: token}
            break;
        case "add": //Agregar cliente
            dataParams = {id: datParametros, type:'add', _token: token}
            break;
        case "activar": //Activar
            dataParams = {id: datParametros, newst:1, _token: token}
            break;
        case "inactivar": //Inactivar
            dataParams = {id: datParametros, newst:0, _token: token}
            break;
    }

    $.ajax({
        url: datUrl,
        type:datMethod,
        dataType:"html",
        data: dataParams,
        success:function(data){
            if (datToogle == "modal") {
                $("#modal_general .modal-content").html(data);
            } else {
                $(".page-content").html(data);
            }
        }
    });
});


