
/**llenado y configuracion del data table**/
function fillDataTable(msg)
{
    $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
    var table = "";
    var dataSet = [];
    for(var i = 0; i < msg['message'].length; i++)
    {
        var estatus_desc = (msg['message'][i]['estatus'] == 1 ? Lang.get('eventos_js.table.fields.status.active') : Lang.get('eventos_js.table.fields.status.inactive'));
        var tipo_fact_desc = (msg['message'][i]['tipo_fact'] == 1 ? Lang.get('eventos_js.table.fields.factura.si') : Lang.get('eventos_js.table.fields.factura.no'));
        dataSet.push({
            "id": msg['message'][i]['id'],
            "nombre": msg['message'][i]['nombre'],
            "rfc": msg['message'][i]['rfc'],
            "tipo_fact": msg['message'][i]['tipo_fact'],
            "tipo_fact_desc": tipo_fact_desc,
            "estatus_desc": estatus_desc,
            "estatus": msg['message'][i]['estatus'],
            "eventos": msg['message'][i]['eventos'],

        } );
    }
    table = $('#example').DataTable( {

        "responsive": true,
        destroy: true,
        "processing": true,
        "language": {
            "lengthMenu": Lang.get('tables_js.table_config_language.lengthMenu'),
            "zeroRecords": Lang.get('tables_js.table_config_language.zeroRecords'),
            "info": Lang.get('tables_js.table_config_language.info'),
            "infoEmpty": Lang.get('tables_js.table_config_language.infoEmpty'),
            "infoFiltered": Lang.get('tables_js.table_config_language.infoFiltered'),
            "search": Lang.get('tables_js.table_config_language.search'),
            "paginate": {
                "first":      Lang.get('tables_js.table_config_language.paginate.first'),
                "last":       Lang.get('tables_js.table_config_language.paginate.last'),
                "next":       Lang.get('tables_js.table_config_language.paginate.next'),
                "previous":   Lang.get('tables_js.table_config_language.paginate.previous')
            }
        },
        data: dataSet,
        columns: [
            { data:"id", title: Lang.get('eventos_js.table.columns.id') },
            { data:"nombre", title: Lang.get('eventos_js.table.columns.nombre') },
            { data:"rfc", title: Lang.get('eventos_js.table.columns.rfc') },
            { data:"tipo_fact_desc", title: Lang.get('eventos_js.table.columns.tipo_fact') },
            { data:"estatus_desc", title: Lang.get('eventos_js.table.columns.name_status') },
            {
                data: null, title: Lang.get('functions.tables.columns.actions'),
                sortable: false,
                "render": function (data, type, full, meta) {
                    //Boton para mostrar información de  un evento
                    botones = '' +
                        '<button data-url="' + url_eve_show + '" data-type="show" data-params="' + full.id + '" ' +
                        'type="button" class="btn btn-margin4r btn-secondary btn-fab btn-sm2 btn-load_view" title="' + Lang.get('functions.functions.show') + '" ><i class="icon-account-search"></i></button>';
                    //Botones para editar un cliente
                    botones = botones +
                        '<button data-url="' + url_eve_show + '" data-type="upd" data-params="' + full.id + '" ' +
                        'type="button" class="btn btn-margin4r btn-secondary btn-fab btn-sm2 btn-load_view" title="' + Lang.get('functions.functions.edit') + '" ><i class="icon-pencil"></i></button>';
                    //Ver productos
                    botones = botones +
                        '<a href="' + url_verproductos + '/' + full.id + '" ' +
                        'class="btn btn-margin4r btn-secondary btn-fab btn-sm2 " title="' + Lang.get('eventos.buttons.ver_productos') + '" ><i class="icon-clipboard-text"></i></a>';
                    return botones;
                }
            }
        ],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": true,
                "searchable": false
            }
        ]
    } );
    /**poder seleccionar una fila**/
    $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );

}

/**transformar información del post en JSON**/
(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery)

/**
 * Envía el token necesario
 */
$(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

});

/**
 * Función que permite cargar vistas Modales de acuerdo a las necesidades
 */
$(document).on("click",".btn-load_view", function(){
    datParametros = $(this).attr("data-params");
    datUrl = $(this).attr("data-url");
    datType = $(this).attr("data-type");
    datToogle = $(this).attr("data-toggle");
    datMethod = $(this).attr("data-method");
    datMethod = (datMethod===undefined)?"POST":datMethod;

    //console.log ("datParametros:" + datParametros + "-datUrl:" + datUrl + "-datType:"  + datType + "-datToogle:"  + datToogle + "-datMethod:"  + datMethod);

    //Obtiene los parámetros necesarios
    dataParams = {};
    switch (datType){
        case "show": //Mostrar client
            dataParams = {id: datParametros, clientes_id: cliente_id, type:'show', _token: token}
            break;
        case "upd": //Actualizar cliente
            dataParams = {id: datParametros, clientes_id: cliente_id, type:'upd', _token: token}
            break;
        case "add": //Agregar cliente
            dataParams = {id: datParametros, clientes_id: cliente_id, type:'add', _token: token}
            break;


        case "activar": //Activar
            dataParams = {id: datParametros, newst:1, _token: token}
            break;
        case "inactivar": //Inactivar
            dataParams = {id: datParametros, newst:2, _token: token}
            break;

    }

    $.ajax({
        url: datUrl,
        type:datMethod,
        dataType:"html",
        data: dataParams,
        success:function(data){
            if (datToogle == "modal") {
                $("#modal_general .modal-content").html(data);
            } else {
                $(".page-content").html(data);
            }
        }
    });
});
