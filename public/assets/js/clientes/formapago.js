/**llenado y configuracion del data table**/
function fillDataTable(msg)
{
    $('#example').DataTable( {
        "responsive": true,
        destroy: true,
        "processing": true,
        "language": {
            "lengthMenu": Lang.get('tables_js.table_config_language.lengthMenu'),
            "zeroRecords": Lang.get('tables_js.table_config_language.zeroRecords'),
            "info": Lang.get('tables_js.table_config_language.info'),
            "infoEmpty": Lang.get('tables_js.table_config_language.infoEmpty'),
            "infoFiltered": Lang.get('tables_js.table_config_language.infoFiltered'),
            "search": Lang.get('tables_js.table_config_language.search'),
            "paginate": {
                "first":      Lang.get('tables_js.table_config_language.paginate.first'),
                "last":       Lang.get('tables_js.table_config_language.paginate.last'),
                "next":       Lang.get('tables_js.table_config_language.paginate.next'),
                "previous":   Lang.get('tables_js.table_config_language.paginate.previous')
            }
        }
    } );

    //ya que llenamos la tabla de formas de pago buscamos cuales estan asignados y cuales no
    getPayment();
}


function getPayment()
{
    $.ajax({
        method: 'POST',
        url: url_set,
        data: {productos_id:producto_id, _token: token}
    })
        .done(function(msg){
            console.log(msg['message']);
            //definimos que checkbox se van a marcar
            var chkBox = "";
            for(var i = 0; i < msg['message'].length; i++) {
                chkBox = $('#checkPay'+msg['message'][i]['clientes_payconfig_id'])
                chkBox.prop('checked', true);
            }
        });
}

function setPayment(id)
{
    //obtenemos el Id del payment
    var mod = $("#checkPay"+id);
    //identificamos si esta checado o no true si se acaba de checar el checkbox, false si se quita la marca del checkbox
    var checked = mod.is( ":checked" );
    $.ajax({
        method: 'POST',
        url: url,
        data: {productos_id:producto_id, payconfig_id: id, flag:checked, _token: token}
    })
        .done(function(msg){
            //console.log(msg['message'])
        });
}