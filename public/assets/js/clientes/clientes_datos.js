

/**llenado y configuracion del data table**/
function fillDataTable(msg)
{
    $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
    var table = "";
    var dataSet = [];
    for(var i = 0; i < msg['message'].length; i++)
    {
        //var name_status = (msg['message'][i]['estatus'] == 1 ? 'Activo' : 'Inactivo');
        var name_status = '';
        switch(msg['message'][i]['obligatorio'])
        {
            case 0:
            case "0":
                name_status = Lang.get('clientes_js.datos.fields.obligatorio.label_no');
                break;
            case 1:
            case "1":
                name_status = Lang.get('clientes_js.datos.fields.obligatorio.label_yes');
                break;
        }
        var name_tipo = '';
        switch(msg['message'][i]['tipo'])
        {
            case 1:
            case "1":
                name_tipo = Lang.get('clientes_js.datos.fields.tipo.label_string');
                break;
            case 2:
            case "2":
                name_tipo = Lang.get('clientes_js.datos.fields.tipo.label_entero');
                break;
            case 3:
            case "3":
                name_tipo = Lang.get('clientes_js.datos.fields.tipo.label_catalogo');
                break;
        }
        //Si se requieren formatear desde este momento, aquí es el lugar preciso
        dataSet.push({
            "id": msg['message'][i]['id'],
            "nombre": msg['message'][i]['nombre'],
            'descripcion': msg['message'][i]['descripcion'],
            'obligatorio': msg['message'][i]['obligatorio'],
            'obligatorio_name': name_status,
            'tipo': msg['message'][i]['tipo'],
            'tipo_name': name_tipo,
        });
    }
    table = $('#example').DataTable( {

        "responsive": true,
        destroy: true,
        "processing": true,
        "language": {
            "lengthMenu": Lang.get('tables_js.table_config_language.lengthMenu'),
            "zeroRecords": Lang.get('tables_js.table_config_language.zeroRecords'),
            "info": Lang.get('tables_js.table_config_language.info'),
            "infoEmpty": Lang.get('tables_js.table_config_language.infoEmpty'),
            "infoFiltered": Lang.get('tables_js.table_config_language.infoFiltered'),
            "search": Lang.get('tables_js.table_config_language.search'),
            "paginate": {
                "first":      Lang.get('tables_js.table_config_language.paginate.first'),
                "last":       Lang.get('tables_js.table_config_language.paginate.last'),
                "next":       Lang.get('tables_js.table_config_language.paginate.next'),
                "previous":   Lang.get('tables_js.table_config_language.paginate.previous')
            }
        },
        data: dataSet,
        columns: [
            { data: 'id', title: Lang.get('clientes_js.table.columns_datos.id') },
            { data: 'nombre', title: Lang.get('clientes_js.table.columns_datos.nombre') },
            { data: 'descripcion', title: Lang.get('clientes_js.table.columns_datos.descripcion') },
            { data: 'obligatorio', title: Lang.get('clientes_js.table.columns_datos.obligatorio') },
            { data: 'obligatorio_name', title: Lang.get('clientes_js.table.columns_datos.obligatorio') },
            { data: 'tipo', title: Lang.get('clientes_js.table.columns_datos.tipo') },
            { data: 'tipo_name', title: Lang.get('clientes_js.table.columns_datos.tipo') },
            { data: null, title: Lang.get('functions.tables.columns.actions'),
                sortable: false,
                "render": function ( data, type, full, meta ) {
                    //Boton para mostrar información de cliente
                    botones = '' +
                        '<button data-url="' + url_adi_show + '" data-type="show" data-params="' + full.id + '" ' +
                        'type="button" class="btn btn-margin4r btn-secondary btn-fab btn-sm2 btn-load_view" title="' + Lang.get('functions.functions.show') + '" ><i class="icon-account-search"></i></button>';
                    //Botón para editar datos de cliente
                    botones = botones +
                        '<button data-url="' + url_adi_show + '" data-type="upd" data-params="' + full.id + '" ' +
                        'type="button" class="btn btn-margin4r btn-secondary btn-fab btn-sm2 btn-load_view" title="' + Lang.get('functions.functions.edit') + '" ><i class="icon-pencil"></i></button>';

                    return botones;
                }

            }

        ],
        columnDefs: [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 3 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 5 ],
                "visible": false,
                "searchable": false
            },
        ]
    } );


    /**transformar información del post en JSON**/
    (function ($) {
        $.fn.serializeFormJSON = function () {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };
    })(jQuery);

}

$(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

});

/**
 * Función que permite cargar vistas Modales de acuerdo a las necesidades
 */
$(document).on("click",".btn-load_view", function(){
    datParametros = $(this).attr("data-params");
    datUrl = $(this).attr("data-url");
    datType = $(this).attr("data-type");
    datToogle = $(this).attr("data-toggle");
    datMethod = $(this).attr("data-method");
    datMethod = (datMethod===undefined)?"POST":datMethod;

    //Obtiene los parámetros necesarios
    dataParams = {};
    switch (datType){
        case "show": //Mostrar client
            dataParams = {id: datParametros, type:'show', _token: token}
            break;
        case "upd": //Actualizar cliente
            dataParams = {id: datParametros, type:'upd', _token: token}
            break;
        case "add": //Agregar cliente
            dataParams = {id: datParametros, type:'add', _token: token}
            break;
        case "messagetmp":
            dataParams = {msg:"Error en módulo", _token: token}
            break;

    }

    $.ajax({
        url: datUrl,
        type:datMethod,
        dataType:"html",
        data: dataParams,
        success:function(data){
            if (datToogle == "modal") {
                $("#modal_general .modal-content").html(data);
            } else {
                $(".page-content").html(data);
            }
        }
    });
});

/**Validador JSON**/
function tryParseJSON (jsonString){
    try {

        if(jsonString == "")
        {
            return true;
        }else
        {
            var o = JSON.parse(jsonString);

            // Handle non-exception-throwing cases:
            // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
            // but... JSON.parse(null) returns null, and typeof null === "object",
            // so we must check for that, too. Thankfully, null is falsey, so this suffices:
            if (o && typeof o === "object") {
                return true;
            }
        }

    }
    catch (e) { }

    return false;
};
