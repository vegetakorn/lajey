

$('#tipoEstatus').on('change' , function () {
    //alert(document.getElementsByName('tipo')[0].value);
    $.ajax({
        method: 'POST',
        url: url,
        data: {estatus:document.getElementsByName('estatus')[0].value,  _token: token}
    })
        .done(function(msg){
            fillDataTable(msg)
        });
});

/**llenado y configuracion del data table**/
function fillDataTable(msg)
{
    $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
    var table = "";
    var dataSet = [];
    for(var i = 0; i < msg['message'].length; i++)
    {
        //var name_status = (msg['message'][i]['estatus'] == 1 ? 'Activo' : 'Inactivo');
        var name_status = '';
        switch(msg['message'][i]['estatus'])
        {
            case 0:
            case "0":
                name_status = Lang.get('clientes_js.table.fields.status.nuevo');
                break;
            case 1:
            case "1":
                name_status = Lang.get('clientes_js.table.fields.status.active');
                break;
            case 2:
            case "2":
                name_status = Lang.get('clientes_js.table.fields.status.inactive');
                break;
            case 3:
            case "3":
                name_status = Lang.get('clientes_js.table.fields.status.moroso');
                break;
        }
        //Si se requieren formatear desde este momento, aquí es el lugar preciso
        dataSet.push({
            "id": msg['message'][i]['id'],
            "nombre": msg['message'][i]['nombre'],
            'alias_evento': msg['message'][i]['alias_evento'],
            'estatus': msg['message'][i]['estatus'],
            'estatus_name': name_status,
        });
    }
    table = $('#example').DataTable( {

        "responsive": true,
        destroy: true,
        "processing": true,
        "language": {
            "lengthMenu": Lang.get('tables_js.table_config_language.lengthMenu'),
            "zeroRecords": Lang.get('tables_js.table_config_language.zeroRecords'),
            "info": Lang.get('tables_js.table_config_language.info'),
            "infoEmpty": Lang.get('tables_js.table_config_language.infoEmpty'),
            "infoFiltered": Lang.get('tables_js.table_config_language.infoFiltered'),
            "search": Lang.get('tables_js.table_config_language.search'),
            "paginate": {
                "first":      Lang.get('tables_js.table_config_language.paginate.first'),
                "last":       Lang.get('tables_js.table_config_language.paginate.last'),
                "next":       Lang.get('tables_js.table_config_language.paginate.next'),
                "previous":   Lang.get('tables_js.table_config_language.paginate.previous')
            }
        },
        data: dataSet,
        columns: [
            { data: 'id', title: Lang.get('clientes_js.table.columns.id') },
            { data: 'nombre', title: Lang.get('clientes_js.table.columns.nombre') },
            { data: 'alias_evento', title: Lang.get('clientes_js.table.columns.alias') },
            { data: 'estatus_name', title: Lang.get('clientes_js.table.columns.name_status') },
            { data: null, title: Lang.get('functions.tables.columns.actions'),
                sortable: false,
                "render": function ( data, type, full, meta ) {
                    //Boton para mostrar información de cliente
                    botones = '' +
                        '<button data-url="' + url_cli_show + '" data-type="show" data-params="' + full.id + '" ' +
                            'type="button" class="btn btn-margin4r btn-secondary btn-fab btn-sm2 btn-load_view" title="' + Lang.get('functions.functions.show') + '" ><i class="icon-account-search"></i></button>';
                    //Botón para editar datos de cliente
                    botones = botones +
                        '<button data-url="' + url_cli_show + '" data-type="upd" data-params="' + full.id + '" ' +
                            'type="button" class="btn btn-margin4r btn-secondary btn-fab btn-sm2 btn-load_view" title="' + Lang.get('functions.functions.edit') + '" ><i class="icon-pencil"></i></button>';
                    //Botones para cambio de estatus
                    if (full.estatus == 0 || full.estatus == 2 || full.estatus == 3) {
                        botones = botones +
                            '<button data-url="' + url_changest_view + '" data-type="activar" data-params="' + full.id + '" ' +
                                'type="button" class="btn btn-margin4r btn-secondary btn-fab btn-sm2 btn-load_view" data-toggle="modal" data-target="#modal_general" title="' + Lang.get('functions.functions.activate') + '" ><i class="icon-check"></i></button>';
                    } else if (full.estatus == 1) {
                        botones = botones +
                            '<button data-url="' + url_changest_view + '" data-type="inactivar" data-params="' + full.id + '" ' +
                            'type="button" class="btn btn-margin4r btn-danger btn-fab btn-sm2 btn-load_view" data-toggle="modal" data-target="#modal_general" title="' + Lang.get('functions.functions.activate') + '" ><i class="icon-close"></i></button>';
                    }
                    //Boton para cambio por morosidad
                    if (full.estatus != 3) {
                        botones = botones +
                            '<button data-url="' + url_changest_view + '" data-type="inactivarmoroso" data-params="' + full.id + '" ' +
                            'type="button" class="btn btn-margin4r btn-danger btn-fab btn-sm2 btn-load_view" data-toggle="modal" data-target="#modal_general" title="' + Lang.get('clientes.buttons.activa_morosidad') + '" ><i class="icon-close-network"></i></button>';
                    }
                    //Ver datos de eventos
                    botones = botones +
                        '<a href="' + url_vereventos + '/' + full.id + '" ' +
                        'class="btn btn-margin4r btn-secondary btn-fab btn-sm2 " title="' + Lang.get('clientes.buttons.infoshow_eventos') + '" ><i class="icon-airplane-takeoff"></i></a>';
                    //Ver datos de facturación
                    botones = botones +
                        '<a href="' + url_verrfcs + '/' + full.id + '" ' +
                        'class="btn btn-margin4r btn-secondary btn-fab btn-sm2 btn-load_view" title="' + Lang.get('clientes.buttons.infoshow_facturacion') + '" ><i class="icon-cash-usd"></i></a>';
                    //Ver definición de datos adicionales
                    botones = botones +
                        '<a href="' + url_adicional + '/' + full.id + '" ' +
                        ' class="btn btn-margin4r btn-secondary btn-fab btn-sm2 " title="' + Lang.get('clientes.buttons.infoshow_adicional') + '" ><i class="icon-account-settings"></i></button>';
                    //Ver definición de datos adicionales
                    botones = botones +
                        '<a href="' + url_payments + '/' + full.id + '" ' +
                        ' class="btn btn-margin4r btn-secondary btn-fab btn-sm2 " title="' + Lang.get('clientes.buttons.infoshow_payments') + '" ><i class="icon-credit-card"></i></button>';
                    //Ver definición de datos adicionales
                    botones = botones +
                        '<a href="' + url_tipo_asoc + '/' + full.id + '" ' +
                        ' class="btn btn-margin4r btn-secondary btn-fab btn-sm2 " title="' + Lang.get('clientes.buttons.infoshow_tipoasoc') + '" ><i class="icon-account-network"></i></button>';

                    return botones;
                }

            }
        ],
        columnDefs: [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
        ]
    } );
    /**poder seleccionar una fila**/
    $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            //$(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            //$(this).addClass('selected');
        }

        //alert(table.row('.selected').data());
        var mystr = "";
        //Loading the variable
        var mystr = table.row('.selected').data()+ ' ';
        //Splitting it with : as the separator
        var myarr = mystr.split(",");
        $('.ajax-btnprod').html( ' <a href="'+url_prod+'/'+myarr[0]+'" > <img  class="pull-right" data-toggle="tooltip" title="Ver Productos" width="35" height="35" src="'+srcprod+'"alt="Icono"> </a>');
        $('.ajax-btnev').html( ' <a href="'+url_ev+'/'+myarr[0]+'" > <img  class="pull-right" data-toggle="tooltip" title="Ver Eventos" width="35" height="35" src="'+srcev+'"alt="Icono"> </a>');
        $('.ajax-btnfac').html( ' <a href="'+url_fac+'/'+myarr[0]+'" > <img  class="pull-right" data-toggle="tooltip" title="Ver Facturacion" width="35" height="35" src="'+srcfac+'"alt="Icono"> </a>');

    } );

    /**transformar información del post en JSON**/
    (function ($) {
        $.fn.serializeFormJSON = function () {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };
    })(jQuery);

}

$(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

});

/**
 * Función que permite cargar vistas Modales de acuerdo a las necesidades
 */
$(document).on("click",".btn-load_view", function(){
    datParametros = $(this).attr("data-params");
    datUrl = $(this).attr("data-url");
    datType = $(this).attr("data-type");
    datToogle = $(this).attr("data-toggle");
    datMethod = $(this).attr("data-method");
    datMethod = (datMethod===undefined)?"POST":datMethod;

    //Obtiene los parámetros necesarios
    dataParams = {};
    switch (datType){
        case "show": //Mostrar client
            dataParams = {id: datParametros, type:'show', _token: token}
            break;
        case "upd": //Actualizar cliente
            dataParams = {id: datParametros, type:'upd', _token: token}
            break;
        case "add": //Agregar cliente
            dataParams = {id: datParametros, type:'add', _token: token}
            break;
        case "activar": //Activar
            dataParams = {id: datParametros, newst:1, _token: token}
            break;
        case "inactivar": //Inactivar
            dataParams = {id: datParametros, newst:2, _token: token}
            break;
        case "inactivarmoroso": //Inactivar por moroso
            dataParams = {id: datParametros, newst:3, _token: token}
            break;
        case "vereventos": //Ver eventos
            dataParams = {id: datParametros}
            break;
        case "ver_rfcs": //Ver Rfcs
            dataParams = {id: datParametros}
            break;
        case "adicional":
            dataParams = {id: datParametros, type:'adi',_token: token}
            break;
        case "messagetmp":
            dataParams = {msg:"Error en módulo", _token: token}
            break;

    }

    $.ajax({
        url: datUrl,
        type:datMethod,
        dataType:"html",
        data: dataParams,
        success:function(data){
            if (datToogle == "modal") {
                $("#modal_general .modal-content").html(data);
            } else {
                $(".page-content").html(data);
            }
        }
    });
});

/**Validador JSON**/
function tryParseJSON (jsonString){
    try {

        if(jsonString == "")
        {
            return true;
        }else
        {
            var o = JSON.parse(jsonString);

            // Handle non-exception-throwing cases:
            // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
            // but... JSON.parse(null) returns null, and typeof null === "object",
            // so we must check for that, too. Thankfully, null is falsey, so this suffices:
            if (o && typeof o === "object") {
                return true;
            }
        }

    }
    catch (e) { }

    return false;
};
