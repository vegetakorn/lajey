

$('#tipoEstatus').on('change' , function () {
    //alert(document.getElementsByName('tipo')[0].value);
    $.ajax({
        method: 'POST',
        url: url,
        data: {estatus:document.getElementsByName('estatus')[0].value,  _token: token}
    })
        .done(function(msg){
            console.log(msg['message'])
            fillDataTable(msg)
        });
});

/**llenado y configuracion del data table**/
function fillDataTable(msg)
{
    $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
    var table = "";
    var dataSet = [];
    for(var i = 0; i < msg['message'].length; i++)
    {
        //var name_status = (msg['message'][i]['estatus'] == 1 ? 'Activo' : 'Inactivo');
        var name_status = '';
        switch(msg['message'][i]['estatus'])
        {
            case 0:
            case "0":
                name_status = Lang.get('clientes_js.table_field_status_nuevo');
                break;
            case 1:
            case "1":
                name_status = Lang.get('clientes_js.table_field_status_active');
                break;
            case 2:
            case "2":
                name_status = Lang.get('clientes_js.table_field_status_inactive');
                break;
            case 3:
            case "3":
                name_status = Lang.get('clientes_js.table_field_status_moroso');
                break;
        }
        dataSet.push([
            msg['message'][i]['id'], //0
            msg['message'][i]['nombre'],//1
            msg['message'][i]['alias_evento'],//2
            name_status,//3
            //msg['message'][i]['logotipo'],//4
            //msg['message'][i]['aud_add_user'],//5
            //msg['message'][i]['aud_add_fh'],//6
            //msg['message'][i]['aud_upd_user'],//7
            //msg['message'][i]['aud_upd_fh'],//8
            msg['message'][i]['estatus'],//5
        ] );
    }
    table = $('#example').DataTable( {

        "responsive": true,
        destroy: true,
        "processing": true,
        "language": {
            "lengthMenu": Lang.get('tables_js.table_config_language.lengthMenu'),
            "zeroRecords": Lang.get('tables_js.table_config_language.zeroRecords'),
            "info": Lang.get('tables_js.table_config_language.info'),
            "infoEmpty": Lang.get('tables_js.table_config_language.infoEmpty'),
            "infoFiltered": Lang.get('tables_js.table_config_language.infoFiltered'),
            "search": Lang.get('tables_js.table_config_language.search'),
            "paginate": {
                "first":      Lang.get('tables_js.table_config_language.paginate.first'),
                "last":       Lang.get('tables_js.table_config_language.paginate.last'),
                "next":       Lang.get('tables_js.table_config_language.paginate.next'),
                "previous":   Lang.get('tables_js.table_config_language.paginate.previous')
            }
        },
        data: dataSet,
        columns: [
            { title: Lang.get('clientes_js.table_columns.id') },
            { title: Lang.get('clientes_js.table_columns.nombre') },
            { title: Lang.get('clientes_js.table_columns.alias') },
            { title: Lang.get('clientes_js.table_columns.name_status') },
            //{ title: Lang.get('clientes_js.table_columns.logotipo') },
            //{ title: Lang.get('clientes_js.table_columns.aud_user') },
            //{ title: Lang.get('clientes_js.table_columns.aud_add_fh') },
            //{ title: Lang.get('clientes_js.table_columns.aud_upd') },
            //{ title: Lang.get('clientes_js.table_columns.aud_upd_fh') },
            { title: Lang.get('clientes_js.table_columns.estatus') }
        ],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 4 ],
                "visible": false,
                "searchable": false
            }
        ]
    } );
    /**poder seleccionar una fila**/
    $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }

        //alert(table.row('.selected').data());
        var mystr = "";
        //Loading the variable
        var mystr = table.row('.selected').data()+ ' ';
        //Splitting it with : as the separator
        var myarr = mystr.split(",");
        $('.ajax-btnprod').html( ' <a href="'+url_prod+'/'+myarr[0]+'" > <img  class="pull-right" data-toggle="tooltip" title="Ver Productos" width="35" height="35" src="'+srcprod+'"alt="Icono"> </a>');
        $('.ajax-btnev').html( ' <a href="'+url_ev+'/'+myarr[0]+'" > <img  class="pull-right" data-toggle="tooltip" title="Ver Eventos" width="35" height="35" src="'+srcev+'"alt="Icono"> </a>');
        $('.ajax-btnfac').html( ' <a href="'+url_fac+'/'+myarr[0]+'" > <img  class="pull-right" data-toggle="tooltip" title="Ver Facturacion" width="35" height="35" src="'+srcfac+'"alt="Icono"> </a>');

    } );


    /**ver usuario**/
    $('#btnVer').on('click' , function () {
        console.log("Estoy aqui");
        var mystr = "";
        if(table.row('.selected').data())
        {
            //Loading the variable
            var mystr = table.row('.selected').data()+ ' ';
            //Splitting it with : as the separator
            var myarr = mystr.split(",");
            //Then read the values from the array where 0 is the first
            var myvar = myarr[0] + ":" + myarr[1];
            //alert(table.row('.selected').data());
            $('.ajax-nombre').html( ' <p class="card-text">'+myarr[1]+'</p>');
            $('.ajax-alias').html( ' <p class="card-text">'+myarr[2]+'</p>');
            $('.ajax-status').html( ' <p class="card-text">'+myarr[3]+'</p>');
            $('.ajax-logotipo').html( ' <p class="card-text">'+myarr[4]+'</p>');
            //  $('.ajax-ins').html( ' <p class="card-text">'+' '+Lang.get('clientes_js.show_table_ins_upd_usuario')+' '+myarr[8]+' '+Lang.get('clientes_js.show_table_ins_upd_usuario')+' '+ myarr[9]+'</p>');
            // $('.ajax-upd').html( ' <p class="card-text">'+' '+Lang.get('clientes_js.show_table_ins_upd_usuario')+' '+myarr[10]+' '+Lang.get('clientes_js.show_table_ins_upd_usuario')+' '+ myarr[11]+'</p>');
            getAuds(get_auds, token, myarr[5], myarr[6], 'ins')
            getAuds(get_auds, token, myarr[7], myarr[8], 'upd')

            $('#ModalView').modal();
        }else
        {
            bootbox.alert(Lang.get('functions.alerts.selection'));
        }

    });

    /**
     * Boton para agregar un cliente
     */
    $('#addCliente').on('click' , function () {

        var data = $("#formAddUser").serializeFormJSON();

        $.ajax({
            method: 'POST',
            url: url_add,
            data: {body: data,  _token: token}
        }).done(function(msg){

            console.log(msg['message']);
            if(msg['status'] == 'fail')
            {
               var errores = '';
                errores +=    (msg['message']['nombre']  ?  ' <p class="alert alert-danger" >'+msg['message']['nombre']+'</p>' : '');
                errores +=    (msg['message']['alias_evento']  ?   ' <p class="alert alert-danger" >'+msg['message']['alias_evento']+'</p>' : '');
                $('.ajax-content').html(errores);
            }else
            {
                var errores = ' ';
                $('.ajax-content').html(errores)
                $('#exampleModalLong').modal('toggle');
                fillDataTable(msg);
            }
        }).fail(function(msg){

            console.log(msg['message']);

        });

    });

    /**
     * Boton para editar usuario
     */
    $('#btnEdit').on('click' , function () {
        var mystr = "";
        var select = "";
        if(table.row('.selected').data())
        {
            //Loading the variable
            var mystr = table.row('.selected').data()+ ' ';
            //Splitting it with : as the separator
            var myarr = mystr.split(",");
            //Then read the values from the array where 0 is the first
            var myvar = myarr[0] + ":" + myarr[1];
            //alert(table.row('.selected').data());
            $('.ajax-id').html( '<input type="hidden" class="form-control" name="id" value="'+myarr[0]+'"/>')
            $('.ajax-estatus').html( '<input type="hidden" class="form-control" name="estatus" value="'+myarr[9]+'"/>')

            $('.ajax-nombre').html( '<input type="text" class="form-control"  name="nombre" aria-describedby="emailHelp"\n' +
                ' value="'+myarr[1]+'" required="true">')
            $('.ajax-alias').html(' <input type="text" class="form-control"  name="alias_evento" \n' +
                '                                   value="'+myarr[2]+'">');
            $('.ajax-fileedit').html(' <input type="text" readonly class="form-control"  name="logotipo" \n' +
                '                                   value="'+myarr[4]+'">');
            $('#ModalEdit').modal();
        }else
        {
            bootbox.alert(Lang.get('functions.alerts.selection'));
        }



    });

    /**
     * Botón para enviar a editar cliente
     */
    $('#updUser').on('click' , function () {
        var data = $("#formUpdCliente").serializeFormJSON();
        $.ajax({
            method: 'POST',
            url: url_edit,
            data: {body: data, _token: token}
        }).done(function(msg){

            console.log(msg['message']);
            if(msg['status'] == 'fail')
            {
                var errores = '';
                errores +=    (msg['message']['nombre']  ?  ' <p class="alert alert-danger" >'+msg['message']['nombre']+'</p>' : '');
                errores +=    (msg['message']['alias_evento']  ?   ' <p class="alert alert-danger" >'+msg['message']['alias_evento']+'</p>' : '');
                $('.ajax-error').html(errores);

            }else
            {
                var errores = ' ';
                $('.ajax-error').html(errores)
                $('#ModalEdit').modal('toggle');
                fillDataTable(msg);
            }
        }).fail(function(msg){

            console.log(msg['message']);

        });
    });

    /**alerta de activar usuario**/
    $('#btnAct').on('click' , function () {

        var mystr = "";
        if(table.row('.selected').data())
        {
            //Loading the variable
            var mystr = table.row('.selected').data()+ ' ';
            //Splitting it with : as the separator
            var myarr = mystr.split(",");
            //Then read the values from the array where 0 is the first
            var stat = myarr[9] ;
            var name_status = "";
            var val_status = "";

            if(stat == 0 || stat == 1)
            {
                var name_status = Lang.get('clientes_js.question_inactivate_desc');
                var val_status = "2";
            }else if(stat == 2 || stat == 3)
            {
                var name_status = Lang.get('clientes_js.question_activate_desc');
                var val_status = "1";
            }
            //alert(table.row('.selected').data());
            $('.ajax-idact').html( '<input type="hidden" class="form-control" name="id" value="'+myarr[0]+'"/>')
            $('.ajax-estatus').html( '<input type="hidden" class="form-control" name="estatus" value="'+val_status+'"/>')
            $('.ajax-pregunta').html(' <h4 >' + Lang.get('clientes_js.question_activate', { 'estatus': name_status }) + '</h4>');
            $('.ajax-titulo').html(' <h5 class="modal-title" id="ModalLongTitle">'+name_status+' '+Lang.get('clientes_js.title_head_modal')+'</h5>');

            $('#ModalActivate').modal();
        }else
        {
            bootbox.alert(Lang.get('functions.alerts.selection'));
        }

    });
    /**proceso de activar/desactivar usuario**/
    $('#actCliente').on('click' , function () {
        var data = $("#formActCliente").serializeFormJSON();
        $.ajax({
            method: 'POST',
            url: url_activate,
            data: {body: data, _token: token}
        }).done(function(msg){

            console.log(msg['message']);
            if(msg['status'] == 'fail')
            {
                console.log(msg['message']);
            }else
            {
                $('#ModalActivate').modal('toggle');
                fillDataTable(msg);
            }
        }).fail(function(msg){

            console.log(msg['message']);

        });
    });
    /**alerta de cliente moroso**/
    $('#btnMor').on('click' , function () {

        var mystr = "";
        if(table.row('.selected').data())
        {
            //Loading the variable
            var mystr = table.row('.selected').data()+ ' ';
            //Splitting it with : as the separator
            var myarr = mystr.split(",");
            //Then read the values from the array where 0 is the first
            var stat = myarr[9] ;
            var name_status = "";
            var val_status = "";

            //alert(table.row('.selected').data());
            $('.ajax-idactmor').html( '<input type="hidden" class="form-control" name="id" value="'+myarr[0]+'"/>')

            $('.ajax-titulomor').html(' <h5 class="modal-title" id="ModalLongTitle">'+Lang.get('clientes_js.question_moroso_desc')+'</h5>');

            $('#ModalMoroso').modal();
        }else
        {
            bootbox.alert(Lang.get('functions.alerts.selection'));
        }

    });
    /**proceso de cliente moroso**/
    $('#morCliente').on('click' , function () {
        var data = $("#formMorCliente").serializeFormJSON();
        $.ajax({
            method: 'POST',
            url: url_mor,
            data: {body: data, _token: token}
        }).done(function(msg){

            console.log(msg['message']);
            if(msg['status'] == 'fail')
            {
                console.log(msg['message']);
            }else
            {
                $('#ModalMoroso').modal('toggle');
                fillDataTable(msg);
            }
        }).fail(function(msg){

            console.log(msg['message']);

        });
    });


    /**transformar información del post en JSON**/
    (function ($) {
        $.fn.serializeFormJSON = function () {

            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };
    })(jQuery);

}


