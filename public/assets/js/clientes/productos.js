
$('#Evento').on('change' , function () {
    //alert(document.getElementsByName('tipo')[0].value);
    $.ajax({
        method: 'POST',
        url: url,
        data: {evento:document.getElementsByName('evento')[0].value, cliente: cliente,   _token: token}
    })
        .done(function(msg){
            console.log(msg['message'])
            fillDataTable(msg)

        });
});

/**llenado y configuracion del data table**/
function fillDataTable(msg)
{
    $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
    var table = "";
    var dataSet = [];
    for(var i = 0; i < msg['message'].length; i++)
    {
        //var reg_primario_desc = Lang.get('productos_js.table.fields.reg_primario.' + msg['message'][i]['reg_primario']);
        var name_status = Lang.get('productos_js.table.fields.estatus.' + msg['message'][i]['estatus']);
        dataSet.push({
            "id": msg['message'][i]['id'],
            "eventos_id": msg['message'][i]['eventos_id'],
            "nombre": msg['message'][i]['nombre'],
            "estatus_desc": name_status,
            "fh_vigencia_ini": msg['message'][i]['fh_vigencia_ini'],
            "fh_vigencia_fin": msg['message'][i]['fh_vigencia_fin'],
            "estatus": msg['message'][i]['estatus'],
           // "prod_id": msg['message'][i]['prod_id'],
           // "reg_primario_desc": reg_primario_desc,
        });
    }
    table = $('#example').DataTable( {

        "responsive": true,
        destroy: true,
        "language": {
            "lengthMenu": Lang.get('tables_js.table_config_language.lengthMenu'),
            "zeroRecords": Lang.get('tables_js.table_config_language.zeroRecords'),
            "info": Lang.get('tables_js.table_config_language.info'),
            "infoEmpty": Lang.get('tables_js.table_config_language.infoEmpty'),
            "infoFiltered": Lang.get('tables_js.table_config_language.infoFiltered'),
            "search": Lang.get('tables_js.table_config_language.search'),
            "paginate": {
                "first":      Lang.get('tables_js.table_config_language.paginate.first'),
                "last":       Lang.get('tables_js.table_config_language.paginate.last'),
                "next":       Lang.get('tables_js.table_config_language.paginate.next'),
                "previous":   Lang.get('tables_js.table_config_language.paginate.previous')
            }
        },
        data: dataSet,
        columns: [
            { data:"id", title: Lang.get('productos_js.table.columns.id') },
            { data:"nombre", title: Lang.get('productos_js.table.columns.nombre') },
            { data:"estatus_desc", title: Lang.get('productos_js.table.columns.estatus')},
          //  { data:"reg_primario_desc", title: Lang.get('productos_js.table.columns.reg_primario')},
          //  { data:"prod_id", title: Lang.get('productos_js.table.columns.prod_id')},
            { data:"fh_vigencia_ini", title: Lang.get('productos_js.table.columns.vigencia_ini')},
            { data:"fh_vigencia_fin", title: Lang.get('productos_js.table.columns.vigencia_fin')},
            {
                data: null, title: Lang.get('functions.tables.columns.actions'),
                sortable: false,
                "render": function (data, type, full, meta) {
                    //Boton para mostrar información
                    botones = '' +
                        '<button data-url="' + url_prod_show + '" data-type="show" data-params="' + full.id + '" ' +
                        'type="button" class="btn btn-margin4r btn-secondary btn-fab btn-sm2 btn-load_view" title="' + Lang.get('functions.functions.show') + '" ><i class="icon-account-search"></i></button>';
                    //Botones para editar
                    botones = botones +
                        '<button data-url="' + url_prod_show + '" data-type="upd" data-params="' + full.id + '" ' +
                        'type="button" class="btn btn-margin4r btn-secondary btn-fab btn-sm2 btn-load_view" title="' + Lang.get('functions.functions.edit') + '" ><i class="icon-pencil"></i></button>';
                    //Botones para cambio de estatus
                    if (full.estatus == 0 ) {
                        botones = botones +
                            '<button data-url="' + url_prod_changest_view + '" data-type="activar" data-params="' + full.id + '" ' +
                            'type="button" class="btn btn-margin4r btn-secondary btn-fab btn-sm2 btn-load_view" data-toggle="modal" data-target="#modal_general" title="' + Lang.get('functions.functions.activate') + '" ><i class="icon-check"></i></button>';
                    } else if (full.estatus == 1) {
                        botones = botones +
                            '<button data-url="' + url_prod_changest_view + '" data-type="inactivar" data-params="' + full.id + '" ' +
                            'type="button" class="btn btn-margin4r btn-danger btn-fab btn-sm2 btn-load_view" data-toggle="modal" data-target="#modal_general" title="' + Lang.get('functions.functions.activate') + '" ><i class="icon-close"></i></button>';
                    }
                    botones = botones +
                        '<a href="' + url_referenciados + '/' + full.id + '" ' +
                        ' class="btn btn-margin4r btn-secondary btn-fab btn-sm2 " title="' + Lang.get('productos_js.buttons.referenciados') + '" ><i class="icon-bulletin-board"></i></button>';
                    botones = botones +
                        '<a href="' + url_formapago + '/' + full.id + '" ' +
                        ' class="btn btn-margin4r btn-secondary btn-fab btn-sm2 " title="' + Lang.get('productos_js.buttons.formapago') + '" ><i class="icon-barcode"></i></button>';
                    botones = botones +
                        '<a href="' + url_subproducto + '/' + full.id + '" ' +
                        ' class="btn btn-margin4r btn-secondary btn-fab btn-sm2 " title="' + Lang.get('productos_js.buttons.subproductos') + '" ><i class="icon-basket-fill"></i></button>';

                    return botones;
                }
            }
        ],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
        ]
    } );

    $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );


}

/**transformar información del post en JSON**/
(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);


/**
 * Envía el token necesario
 */
$(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

});

/**
 * Función que permite cargar vistas Modales de acuerdo a las necesidades
 */
$(document).on("click",".btn-load_view", function(){
    datParametros = $(this).attr("data-params");
    datUrl = $(this).attr("data-url");
    datType = $(this).attr("data-type");
    datToogle = $(this).attr("data-toggle");
    datMethod = $(this).attr("data-method");
    datMethod = (datMethod===undefined)?"POST":datMethod;

    //Obtiene los parámetros necesarios
    dataParams = {};
    switch (datType){
        case "show": //Mostrar client
            dataParams = {id: datParametros, eventos_id: eventos_id, type:'show', _token: token}
            break;
        case "upd": //Actualizar cliente
            dataParams = {id: datParametros, eventos_id: eventos_id, type:'upd', _token: token}
            break;
        case "add": //Agregar cliente
            dataParams = {id: datParametros, eventos_id: eventos_id, type:'add', _token: token}
            break;
        case "activar": //Activar
            dataParams = {id: datParametros, newst:1, eventos_id: eventos_id, _token: token}
            break;
        case "inactivar": //Inactivar
            dataParams = {id: datParametros, newst:0, eventos_id: eventos_id, _token: token}
            break;
    }

    $.ajax({
        url: datUrl,
        type:datMethod,
        dataType:"html",
        data: dataParams,
        success:function(data){
            if (datToogle == "modal") {
                $("#modal_general .modal-content").html(data);
            } else {
                $(".page-content").html(data);
            }
        }
    });
});
