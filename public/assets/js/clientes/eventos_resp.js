$('#optionsRadios2').on('click' , function () {
    $('#selRFC').attr("disabled", true);
});

$('#optionsRadios1').on('click' , function () {
    $('#selRFC').attr("disabled", false);
});
$('#optionsRadios2Edit').on('click' , function () {
    $('#selRFCEdit').attr("disabled", true);
});

$('#optionsRadios1Edit').on('click' , function () {
    $('#selRFCEdit').attr("disabled", false);
});

/**llenado y configuracion del data table**/
function fillDataTable(msg)
{
    $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
    var table = "";
    var dataSet = [];
    for(var i = 0; i < msg['message'].length; i++)
    {
        var name_status = (msg['message'][i]['estatus'] == 1 ? Lang.get('eventos_js.table_field_status_active') : Lang.get('eventos_js.table_field_status_inactive'));
        var tipo_fact = (msg['message'][i]['tipo_fact'] == 1 ? Lang.get('eventos_js.table_field_si_factura') : Lang.get('eventos_js.table_field_no_factura'));
        dataSet.push([
            msg['message'][i]['id'],
            msg['message'][i]['nombre'],
            msg['message'][i]['rfc'],
            tipo_fact,
            name_status,
            msg['message'][i]['estatus'],
            msg['message'][i]['clientes_fact_id'],
            msg['message'][i]['tipo_fact'],
            msg['message'][i]['aud_add_user'],
            msg['message'][i]['aud_add_fh'],
            msg['message'][i]['aud_upd_user'],
            msg['message'][i]['aud_upd_fh'],
        ] );
    }
    table = $('#example').DataTable( {

        "responsive": true,
        destroy: true,
        "processing": true,
        "language": {
            "lengthMenu": Lang.get('tables_js.table_config_language.lengthMenu'),
            "zeroRecords": Lang.get('tables_js.table_config_language.zeroRecords'),
            "info": Lang.get('tables_js.table_config_language.info'),
            "infoEmpty": Lang.get('tables_js.table_config_language.infoEmpty'),
            "infoFiltered": Lang.get('tables_js.table_config_language.infoFiltered'),
            "search": Lang.get('tables_js.table_config_language.search'),
            "paginate": {
                "first":      Lang.get('tables_js.table_config_language.paginate.first'),
                "last":       Lang.get('tables_js.table_config_language.paginate.last'),
                "next":       Lang.get('tables_js.table_config_language.paginate.next'),
                "previous":   Lang.get('tables_js.table_config_language.paginate.previous')
            }
        },
        data: dataSet,
        columns: [
            { title: Lang.get('eventos_js.table_columns.id') },
            { title: Lang.get('eventos_js.table_columns.nombre') },
            { title: Lang.get('eventos_js.table_columns.rfc') },
            { title: Lang.get('eventos_js.table_columns.tipo_fact') },
            { title: Lang.get('eventos_js.table_columns.name_status') },
            { title: Lang.get('eventos_js.table_columns.estatus') },
            { title: Lang.get('eventos_js.table_columns.clientes_fact_id') },
            { title: Lang.get('eventos_js.table_columns.tipo_fact_id') },
            { title: Lang.get('users_js.table_columns.aud_user') },
            { title: Lang.get('users_js.table_columns.aud_add_fh') },
            { title: Lang.get('users_js.table_columns.aud_upd') },
            { title: Lang.get('users_js.table_columns.aud_upd_fh') },


        ],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 5 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 6 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 7 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 8 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 9 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 10 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [ 11 ],
                "visible": false,
                "searchable": false
            },
        ]
    } );
    /**poder seleccionar una fila**/
    $('#example tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );

    $('#addEvento').on('click' , function () {

        if($('input[name=tipo_fact]:checked').val() == 0)
        {
          add();
        }else
        {
            if($('#selRFC option:selected').val() == 0)
            {
                $('.ajax-content').html(' <p class="alert alert-danger" >' + Lang.get('eventos_js.new_validation_cliente_error') + '</p>')
            }else
            {
                $('.ajax-content').html('');
                add();
            }
        }
    });
    function add()
    {
        var data = $("#formAddEvento").serializeFormJSON();

        $.ajax({
            method: 'POST',
            url: url_add,
            data: {body: data,  _token: token}
        }).done(function(msg){

            console.log(msg['message']);
            if(msg['status'] == 'fail')
            {
                var errores = '';
                errores +=    (msg['message']['nombre']  ?  ' <p class="alert alert-danger" >'+msg['message']['nombre']+'</p>' : '');
                $('.ajax-content').html(errores);
            }else
            {
                var errores = ' ';
                $('.ajax-content').html(errores)
                $('#exampleModalLong').modal('toggle');
                fillDataTable(msg);
            }
        }).fail(function(msg){

            console.log(msg['message']);

        });
    }


  /*  $('#addEvento').on('click' , function () {



    });*/
    /**ver evento**/
    $('#btnVer').on('click' , function () {
        var mystr = "";
        if(table.row('.selected').data())
        {
            //Loading the variable
            var mystr = table.row('.selected').data()+ ' ';
            //Splitting it with : as the separator
            var myarr = mystr.split(",");
            //Then read the values from the array where 0 is the first
            var myvar = myarr[0] + ":" + myarr[1];
            //alert(table.row('.selected').data());
            $('.ajax-nombre').html( ' <p class="card-text">'+myarr[1]+'</p>');
            $('.ajax-rfc').html( ' <p class="card-text">'+myarr[2]+'</p>');
            $('.ajax-tipo').html( ' <p class="card-text">'+myarr[3]+'</p>');
            $('.ajax-estatus').html( ' <p class="card-text">'+myarr[4]+'</p>');

           // $('.ajax-ins').html( ' <p class="card-text">'+' '+Lang.get('eventos_js.show_table_ins_upd_usuario')+' '+myarr[8]+' '+Lang.get('eventos_js.show_table_ins_upd_usuario')+' '+ myarr[9]+'</p>');
           // $('.ajax-upd').html( ' <p class="card-text">'+' '+Lang.get('eventos_js.show_table_ins_upd_usuario')+' '+myarr[10]+' '+Lang.get('eventos_js.show_table_ins_upd_usuario')+' '+ myarr[11]+'</p>');
            getAuds(get_auds, token, myarr[8], myarr[9], 'ins')
            getAuds(get_auds, token, myarr[10], myarr[11], 'upd')

            $('#ModalView').modal();
        }else
        {
            bootbox.alert(Lang.get('functions.alert_selection'));
        }

    });
    /**alerta de activar evento**/
    $('#btnAct').on('click' , function () {

        var mystr = "";
        if(table.row('.selected').data())
        {
            //Loading the variable
            var mystr = table.row('.selected').data()+ ' ';
            //Splitting it with : as the separator
            var myarr = mystr.split(",");
            //Then read the values from the array where 0 is the first
            var stat = myarr[5] ;
            var name_status = "";
            var val_status = "";

            if( stat == 1)
            {
                var name_status = Lang.get('eventos_js.question_inactivate_desc');
                var val_status = "0";
            }else if(stat == 0)
            {
                var name_status = Lang.get('eventos_js.question_activate_desc');
                var val_status = "1";
            }
            //alert(table.row('.selected').data());
            $('.ajax-idact').html( '<input type="hidden" class="form-control" name="id" value="'+myarr[0]+'"/>')
            $('.ajax-estatus').html( '<input type="hidden" class="form-control" name="estatus" value="'+val_status+'"/>')
            $('.ajax-pregunta').html(' <h4 >' + Lang.get('eventos_js.question_activate', { 'estatus': name_status }) + '</h4>');
            $('.ajax-titulo').html(' <h5 class="modal-title" id="ModalLongTitle">'+name_status+' '+Lang.get('eventos_js.title_head_modal')+'</h5>');

            $('#ModalActivate').modal();
        }else
        {
            bootbox.alert(Lang.get('functions.alert_selection'));
        }

    });

    /**proceso de activar/desactivar usuario**/
    $('#actEv').on('click' , function () {
        var data = $("#formActEv").serializeFormJSON();
        $.ajax({
            method: 'POST',
            url: url_activate,
            data: {body: data, _token: token}
        }).done(function(msg){

            console.log(msg['message']);
            if(msg['status'] == 'fail')
            {
                console.log(msg['message']);
            }else
            {
                $('#ModalActivate').modal('toggle');
                fillDataTable(msg);
            }
        }).fail(function(msg){

            console.log(msg['message']);

        });
    });



    /**editar evento**/
    $('#btnEdit').on('click' , function () {
        var mystr = "";
        var select = "";
        if(table.row('.selected').data())
        {
            //Loading the variable
            var mystr = table.row('.selected').data()+ ' ';
            //Splitting it with : as the separator
            var myarr = mystr.split(",");
            //Then read the values from the array where 0 is the first
            var myvar = myarr[0] + ":" + myarr[1];
            //alert(table.row('.selected').data());
            $('.ajax-id').html( '<input type="hidden" class="form-control" name="id" value="'+myarr[0]+'"/>')

            $('.ajax-nombreedit').html( '<input type="text" class="form-control"  name="nombre" aria-describedby="emailHelp"\n' +
                ' value="'+myarr[1]+'" required="true">')
            if(myarr[7] == 1)
            {
                $('#selection').replaceWith(' <option value="'+myarr[6]+'" selected>'+myarr[2]+'</option>');
                $("#optionsRadios1Edit").attr("checked", true);
                $("#optionsRadios2Edit").attr("checked", false);
            }else
            {
                $('#selection').replaceWith(' <option value="'+myarr[6]+'" selected>'+myarr[2]+'</option>');
                $("#optionsRadios1Edit").attr("checked", false);
                $("#optionsRadios2Edit").attr("checked", true);
            }
            $('#ModalEdit').modal();
        }else
        {
            bootbox.alert(Lang.get('functions.alert_selection'));
        }



    });
    /**filtro por tipo de usuario**/
    $('#updEv').on('click' , function () {

        if($('input[name=tipo_fact]:checked').val() == 0)
        {
            edit();
        }else
        {
            if($('#selRFCEdit option:selected').val() == 0)
            {
                $('.ajax-error').html(' <p class="alert alert-danger" >' + Lang.get('eventos_js.new_validation_cliente_error') + '</p>')
            }else
            {
                $('.ajax-error').html('');
                edit();
            }
        }
    });
    function edit()
    {
        var data = $("#formUpdAct").serializeFormJSON();
        $.ajax({
            method: 'POST',
            url: url_edit,
            data: {body: data, _token: token}
        }).done(function(msg){

            console.log(msg['message']);
            if(msg['status'] == 'fail')
            {
                var errores = '';
                errores +=    (msg['message']['nombre']  ?  ' <p class="alert alert-danger" >'+msg['message']['nombre']+'</p>' : '');
                $('.ajax-content').html(errores);
            }else
            {
                var errores = ' ';
                $('.ajax-error').html(errores)
                $('#ModalEdit').modal('toggle');
                fillDataTable(msg);
            }
        }).fail(function(msg){

            console.log(msg['message']);

        });
    }



}

/**transformar información del post en JSON**/
(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);