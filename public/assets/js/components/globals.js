function getAuds(url, token, aud_id, aud_fecha, type)
{
    $.ajax({
        method: 'POST',
        url: url,
        data: {body: aud_id, _token: token}
    }).done(function(msg){
        console.log(msg['message']);


        if(type == 'ins')
        {
            $('.ajax-ins').html( ' <p class="card-text">'+' '+Lang.get('clientes_js.show_table_ins_upd_usuario')+' '+msg['message']['nombre']+' '+Lang.get('clientes_js.show_table_ins_upd_fecha')+' '+ aud_fecha+'</p>');
        }else
        {
            $('.ajax-upd').html( ' <p class="card-text">'+' '+Lang.get('clientes_js.show_table_ins_upd_usuario')+' '+msg['message']['nombre']+' '+Lang.get('clientes_js.show_table_ins_upd_fecha')+' '+ aud_fecha+'</p>');
        }
    });
}