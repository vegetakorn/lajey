@section('title_prefix')
    SoyLaJey::Blog
@endsection
@section('blog_active')
    class="active"
@endsection
@extends('welcome')
@section('body')

    <div class="content">
        <div class="single">
            <div class="single-top">
                <script src="js/responsiveslides.min.js"></script>
                <script>
                    $(function () {
                        $("#slider4").responsiveSlides({
                            auto: true,
                            speed: 500,
                            namespace: "callbacks",
                            pager: true,
                        });
                    });
                </script>
                <div class="slider">
                    <div class="callbacks_container">
                        <ul class="rslides" id="slider4">
                            <li>
                                @if($contenido->youtube  != "0")
                                    <iframe width="560" height="315" src="{{$contenido->youtube}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                @else
                                <img src="{{ asset('images/'.$contenido->imagen)}}" alt="">
                                @endif


                            </li>
                        </ul>
                    </div>
                </div>
                <h2>{{$contenido->titulo}}</h2>
                <p class="para">

                    {!! $contenido->larga !!}

                <div class="comment-grid-top">
                    <h3>{{$contenido->coments}} Comentarios</h3>
                    @foreach($comentarios as $comentario)
                        <div class="comments-top-top">
                            <div class="top-comment-left">
                                <img  src="images/co.jpg" alt="">
                            </div>
                            <div class="top-comment-right">
                                <ul>
                                    <li><span > <a href="#">{{$comentario->autor}}</a></span></li>
                                    <li><span >/ {{$comentario->fecha}}</span></li>
                                </ul>
                                <p>{{$comentario->comentario}}</p>
                            </div>
                        <div class="clear"> </div>
                        <label> </label>
                        </div>
                    @endforeach


                </div>
                <div class="top-single">
                    <h3>¡Deja tu comentario!</h3>
                    <div class="grid-single">
                        <div class="your-single">
                            <i> </i>
                            <input type="text" value="Anonimo" id="inputNombre" onfocus="this.value='';" onblur="if (this.value == '') {this.value = 'Anonimo';}">
                            <div class="clear"> </div>
                        </div>

                    </div>
                    <div class="grid-single-in">
                        <textarea cols="77" rows="5" value=" " id="inputMsj" onfocus="this.value='';" onblur="if (this.value == '') {this.value = 'Message';}">Comenta aquí</textarea>
                        <input type="submit" onclick="sendCom()" value="TU COMENTARIO">
                    </div>
                    <div class="clear"> </div>
                </div>
            </div>
            <div class="single-in">
                <div class="info">
                    <h3>{{$contenido->corta}} </h3>
                    <ul class="likes">
                        <li><span><i class="dec"> </i>{{$contenido->fecha}} </span></li>
                        <li><a href="#"><i class="comment"> </i>{{$contenido->coments}}  Comentarios</a></li>
                    </ul>
                </div>

            </div>
            <div class="clear"> </div>
        </div>
    </div>
@endsection
@section('js')

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="{{ asset('alert/bootbox.min.js') }}" ></script>
    <script src="{{ asset('alert/bootbox.locales.js') }}" ></script>
    <script>
        function sendCom()
        {
            //bootbox.alert("Enviando mensaje...");


            if(document.getElementById('inputMsj').value != 'Comenta aquí' && document.getElementById('inputMsj').value != "")
            {
                bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Enviando comentario...</div>' })
                var url = '{{route('sendComent')}}';
                var token = '{{csrf_token()}}';
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {
                        nombre: document.getElementById('inputNombre').value,
                        coments: '{{$contenido->coments}}',
                        id: '{{$contenido->id}}',
                        msj: document.getElementById('inputMsj').value,
                        _token: token
                    }
                })
                    .done(function(msg){
                        console.log(msg);
                        bootbox.alert("¡Comentario enviado! ");
                        document.getElementById('inputNombre').value = "";
                        document.getElementById('inputMsj').value = "";
                        var url = "";
                        url = '<?php echo e(route('blog.contenido', $contenido->id)); ?>';//'visitas/final/'+post_id;//
                        window.location.href = url;

                    });
            }else
            {
                bootbox.alert("¡Olvidas lo más importante!, ¡Escribe todo lo que me tengas que decir!");
            }




        }

    </script>
@endsection