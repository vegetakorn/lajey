@section('title_prefix')
    SoyLaJey::Todo se puede
@endsection
@section('todo_active')
    class="active"
@endsection
@extends('welcome')
@section('body')

    <div class="content">
        <div class="work">
            <div class="work-in">

                <div class="gallery">
                    <h3>Todo se puede</h3>
                    <ul class="gallery-grid">
                        @foreach($fotos as $foto)
                            <li>
                                <a ><img  src="images/{{$foto->foto}}" alt=""></a>
                            </li>
                        @endforeach
                        <div class="clear"> </div>
                    </ul>
                </div>
                <div class="feature">
                    <h3>Todo lo que hago</h3>
                    <ul class="feature-grid">
                        <li><a ><i > </i> Shows de Standup  </a></li>
                        <li><a ><i >  </i>Shows de cabaret</a></li>
                        <li><a ><i> </i>Shows cumbiancheros con "Las chicas super guapachosas"</a></li>
                        <li><a ><i >  </i> Talleres</a></li>
                        <li><a ><i >  </i>Capacitaciones</a></li>
                        <li><a ><i >  </i>Conferencias</a></li>
                        <li><a ><i >  </i>Dinámicas de integración para empresas</a></li>

                    </ul>
                </div>
                <p style="color:#000">Escribo, actúo,  hago reír, hago llorar, hago sentir, hago pensar, puedo ser locutora, conductora, bailarina en festivales escolares y así... </p>
            </div>
            <div class="clear"> </div>
        </div>
    </div>
@endsection