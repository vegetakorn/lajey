@section('title_prefix')
    SoyLaJey::Gestión de blog
@endsection
@section('f_blog_active')
    class="active"
@endsection
@extends('welcome')
@section('body')

    <div class="content">
        <div class="contact">
            <div class="grid-contact-in pull-right-lg">

               <a href="{{route('addBlog')}}"><input type="submit"  value="Crear blog"></a>
            </div>
            <table class="table">
                <tr>
                    <th>Titulo</th>
                    <th>Fecha</th>
                    <th>Comentarios</th>
                    <th></th>
                    <th></th>
                </tr>
                @foreach($blogs as $blog)
                    <tr>
                        <td>{{$blog->titulo}}</td>
                        <td>{{$blog->fecha}}</td>
                        <td>{{$blog->coments}}</td>
                        <td><a href="{{route('editBlog', $blog->id)}}"  >Editar</a></td>
                        <td><a href="#" onclick="borraBlog({{$blog->id}})" >Borrar</a></td>
                    </tr>
                @endforeach


            </table>
        </div>

    </div>



@endsection
@section('js')

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="{{ asset('alert/bootbox.min.js') }}" ></script>
    <script src="{{ asset('alert/bootbox.locales.js') }}" ></script>
    <script>

        function borraBlog(id)
        {
            var url = '{{route('delBlog')}}';
            var token = '{{csrf_token()}}';
            $.ajax({
                method: 'POST',
                url: url,
                data: {
                    id: id,
                    _token: token
                }
            })
                .done(function(msg){
                    console.log(msg);
                    var url = "";
                    url = '<?php echo e(route('blog_admin')); ?>';//'visitas/final/'+post_id;//
                    window.location.href = url;

                });
        }


        function editarBlog(id)
        {

        }

        function sendMsj()
        {
            //bootbox.alert("Enviando mensaje...");

            if(document.getElementById('inputNombre').value != 'Nombre' && document.getElementById('inputNombre').value != "")
            {
                if(document.getElementById('inputMail').value != 'E-mail' && document.getElementById('inputMail').value != "")
                {
                    if(document.getElementById('inputMsj').value != 'Escribe aquí tu mensaje' && document.getElementById('inputMsj').value != "")
                    {
                        bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Enviando mensaje...</div>' })
                        var url = '{{route('sendMail')}}';
                        var token = '{{csrf_token()}}';
                        $.ajax({
                            method: 'POST',
                            url: url,
                            data: {
                                nombre: document.getElementById('inputNombre').value,
                                mail: document.getElementById('inputMail').value,
                                page: document.getElementById('inputPage').value,
                                msj: document.getElementById('inputMsj').value,
                                _token: token
                            }
                        })
                            .done(function(msg){
                                console.log(msg);
                                var url = "";
                                url = '<?php echo e(route('blog_admin')); ?>';//'visitas/final/'+post_id;//
                                window.location.href = url;



                            });
                    }else
                    {
                        bootbox.alert("¡Olvidas lo más importante!, ¡Escribe todo lo que me tengas que decir!");
                    }

                }else
                {
                    bootbox.alert("Me gustaría poder contactarte, por lo menos deja un correo electrónico válido");
                }

            }else
            {
                bootbox.alert("¡Déjame saber quién eres! Sin pena pon tu nombre.");
            }



        }

    </script>
@endsection