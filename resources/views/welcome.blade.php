<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/

-->
<!DOCTYPE html>
<html>
<head>
    <title>@yield('title_prefix')</title>
    <!-- jQuery-->
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <!--CK editor-->
    <script src="{{ asset('ckeditor/ckeditor.js')}}"></script>
    @include('ckfinder::setup')
    <!-- Custom Theme files -->
    <!--theme-style-->
    <link href="{{ asset('css/style.css')}}" rel="stylesheet" type="text/css" media="all" />

    <!--//theme-style-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="standup, soylajey, aderith, teatro, actriz" />

    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--fonts-->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900' rel='stylesheet' type='text/css'>
    <!--//fonts-->
</head>
<body>
<div class="header">

    @include('layout.header_left')
    @include('layout.header_top')
    @yield('body')

    <div class="clear"> </div>
    @include('layout.footer')

</div>


@yield('js')
</body>
</html>