@section('title_prefix')
    SoyLaJey::Contacto
@endsection
@section('contacto_active')
    class="active"
@endsection
@extends('welcome')
@section('body')

    <div class="content">
        <div class="contact">
            <h1>¡Escríbeme!</h1>
            <p>
                No te dejo mi teléfono porque una nunca sabe, pero puedes dejarme por aquí tu mensaje.
                <span>
                    Si no te contesto seguro te fuiste a Spam y entonces será mejor que me mandes un mensaje privado en mis redes sociales.
                </span>
                <span>
                    ¡Gracias por existir y llegar hasta aquí! .
                </span>

            </p>
            <div class="top-contact">
                <h3>Deja aquí tu mensaje</h3>
                <div class="grid-contact">
                    <div class="your-top">
                        <i> </i>
                        <input type="text" id="inputNombre" value="Nombre" onfocus="this.value='';" onblur="if (this.value == '') {this.value = 'Nombre';}">
                        <div class="clear"> </div>
                    </div>
                    <div class="your-top">
                        <i class="email"> </i>
                        <input type="text" id="inputMail" value="E-mail" onfocus="this.value='';" onblur="if (this.value == '') {this.value = 'E-mail';}">
                        <div class="clear"> </div>
                    </div>
                    <div class="your-top">
                        <i class="website"> </i>
                        <input type="text" id="inputPage" value="Tu página" onfocus="this.value='';" onblur="if (this.value == '') {this.value = 'Tu página';}">
                        <div class="clear"> </div>
                    </div>

                </div>
                <div class="grid-contact-in">
                    <textarea cols="77" id="inputMsj" rows="5" value=" " onfocus="this.value='';" onblur="if (this.value == '') {this.value = 'Escribe aquí tu mensaje';}">Escribe aquí tu mensaje</textarea>
                    <input type="submit" onclick="sendMsj()" value="¡Envíalo!">
                </div>
                <div class="clear"> </div>
            </div>
        </div>
        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d18423.084034669748!2d-99.13948547973779!3d19.42449000519478!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2smx!4v1559549982191!5m2!1ses-419!2smx"></iframe>

        </div>
    </div>
@endsection
@section('js')

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="{{ asset('alert/bootbox.min.js') }}" ></script>
    <script src="{{ asset('alert/bootbox.locales.js') }}" ></script>
    <script>
        function sendMsj()
        {
            //bootbox.alert("Enviando mensaje...");

            if(document.getElementById('inputNombre').value != 'Nombre' && document.getElementById('inputNombre').value != "")
            {
                if(document.getElementById('inputMail').value != 'E-mail' && document.getElementById('inputMail').value != "")
                {
                    if(document.getElementById('inputMsj').value != 'Escribe aquí tu mensaje' && document.getElementById('inputMsj').value != "")
                    {
                        bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Enviando mensaje...</div>' })
                        var url = '{{route('sendMail')}}';
                        var token = '{{csrf_token()}}';
                        $.ajax({
                            method: 'POST',
                            url: url,
                            data: {
                                nombre: document.getElementById('inputNombre').value,
                                mail: document.getElementById('inputMail').value,
                                page: document.getElementById('inputPage').value,
                                msj: document.getElementById('inputMsj').value,
                                _token: token
                            }
                        })
                            .done(function(msg){
                                console.log(msg);
                                bootbox.alert("¡Mensaje enviado! pronto estaremos en contacto...");
                                document.getElementById('inputNombre').value = "";
                                document.getElementById('inputMail').value = "";
                                document.getElementById('inputPage').value = "";
                                document.getElementById('inputMsj').value = "";
                                bootbox.hideAll();



                            });
                    }else
                    {
                        bootbox.alert("¡Olvidas lo más importante!, ¡Escribe todo lo que me tengas que decir!");
                    }

                }else
                {
                    bootbox.alert("Me gustaría poder contactarte, por lo menos deja un correo electrónico válido");
                }

            }else
            {
                bootbox.alert("¡Déjame saber quién eres! Sin pena pon tu nombre.");
            }



        }

    </script>
@endsection