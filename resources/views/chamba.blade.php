@section('title_prefix')
    SoyLaJey::La Chamba
@endsection
@section('chamba_active')
    class="active"
@endsection
@extends('welcome')
@section('body')

    <div class="content">
        <div class="work">
            <div class="work-top">
                <script src="js/responsiveslides.min.js"></script>
                <script>
                    $(function () {
                        $("#slider").responsiveSlides({
                            auto: true,
                            speed: 500,
                            namespace: "callbacks",
                            pager: true,
                        });
                    });
                </script>
                <div  class="slider">
                    <div class="callbacks_container">
                        <ul class="rslides" id="slider">
                            @foreach($fotos as $foto)
                                <li>
                                    <img  src="images/{{$foto->foto}}" alt="">
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

            </div>
            <div class="work-in">
                <h2><a >La Chamba</a></h2>
                <p>
                    Actúo, escribo y hago comedia  y soy parte de un grupo de cumbias llamado "Las chicas super guapachosas" con el que tengo un show cómico-musical.
                    <span> En el teatro he trabajo con la Compañía de Teatro Penitenciario y un par de proyectos de impacto social del Foro Shakespeare, entre otros.  Hago drama, comedia, cabaret y todo lo que me pida la vida. </span>
                    <span>Para la pantalla, he colaborado en E-entertaiment, Comedy Central y tengo por ahí un comercial. También he participado en cortometrajes y he grabado cursos de capacitación para diferentes empresas.  </span>
                    <span>En la comedia he grabado para Comedy Central y he sido y soy parte de diferentes colectivos de comedia de Stad Up entre ellos "La lotería del Stand Up" y "Morras Stand Up" He tenido temporadas en diferentes espacios como Foro Shakespeare, Beer Hall, Foro Culebra y El 77 Centro Cultural Autogestivo y me he presentado en diferentes escenarios de la República Mexicana.</span>
                    <span> Fui parte de una gira llevando stand up a  Reclusorios y Penitenciarías en la CDMX y también fui asistente de Gloria Rodríguez en un taller dentro del reclusorio femenil en Chiconautla con la finalidad de canalizar la energía de las internas en la comedia. </span>
                    <span>He sido parte de la alineación en diferentes festivales como:
                        “Secretos de Sócrates”, “Festival FICTA”, “Festival Marvin” y “Comedy Fest”.</span>
                </p>
            </div>
            <div class="clear"> </div>
        </div>
    </div>
@endsection