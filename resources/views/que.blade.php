@section('title_prefix')
    SoyLaJey::Qué o Qué
@endsection
@section('que_active')
    class="active"
@endsection
@extends('welcome')
@section('body')

    <div class="content">
        <div class="work">
            <div class="work-full">

                <a style="position: center"><img style="position: center; float: right" src="images/{{$fotos->foto}}" alt=""></a>

                <h2><a >¿Qué o qué?</a></h2>
                <p>
                    <span>
                        Puedo ir a tus eventos:
                    </span>
                    <span>
                        Reuniones privadas, shows empresariales, bodas, quince años, despedidas de solteras... También puedo colaborar con corporativos compartiendo herramientas para contribuir con la mejora de la comunicación a través de diferentes dinámicas
                        vinculadas con el arte, la comedia y la actuación.
                    </span>
                </p>
            </div>
            <div class="clear"> </div>
        </div>

    </div>
@endsection