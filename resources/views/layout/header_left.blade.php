<!---->
<div class="header-left">
    <div class="logo">
        <a href="/"><img src="{{ asset('images/logo.png')}}" alt=""></a>
    </div>
    <div class="top-nav">
        <ul >
            <li @yield('inicio_active') ><a href="/" >Jey Aderith</a></li>
            <li @yield('chamba_active')><a href="{{route('chamba')}}" class="black" >La Chamba</a></li>
            <li @yield('que_active')><a href="{{route('que')}}" class="black1">¿Qué o qué?</a></li>
            <li @yield('blog_active')><a href="{{route('blog')}}" class="black2" >Comedy Blog</a></li>
            <li @yield('todo_active')><a href="{{route('todo')}}" class="black3" >Todo se puede</a></li>
            <li @yield('contacto_active')><a href="{{route('contacto')}}" class="black4" >Escríbeme</a></li>
            @if(Auth::check())
                <li @yield('f_inicio_active') ><a href="{{route('edit_inicio')}}" >Fotos de inicio</a></li>
                <li @yield('f_chamba_active') ><a href="{{route('edit_chamba')}}"  >Fotos de chamba</a></li>
                <li @yield('f_que_active') ><a href="{{route('edit_que')}}" >Fotos de qué o qué</a></li>
                <li @yield('f_blog_active') ><a href="{{route('blog_admin')}}" >Gestión blog</a></li>
                <li @yield('f_todo_active') ><a href="{{route('edit_todo')}}"  >Fotos de todo se puede</a></li>
                <li @yield('f_salir_active') ><a href="{{route('salir')}}" >Salir de admin</a></li>
            @endif
        </ul>
    </div>
    <ul class="social-in">

        <li><a href="https://www.facebook.com/pg/Soylajey/about/?ref=page_internal " target="_blank"><i class="facebook"> </i></a></li>
        <li><a href="https://www.youtube.com/channel/UC-U5eqGsagTNaXpywaXosZA" target="_blank"><i class="pin"> </i></a></li>
        <li><a href="https://twitter.com/soylaJey" target="_blank"><i class="twitter"> </i></a></li>
        <li><a href="https://www.instagram.com/soylajey/" target="_blank"><i class="dribble"> </i></a></li>

    </ul>
    <p class="footer-class">Diseño hecho en  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
    <p class="footer-class">Programado por  <a href="http://hdam.mx/" target="_blank">Héctor Arroyo</a> </p>
</div>
<!---->