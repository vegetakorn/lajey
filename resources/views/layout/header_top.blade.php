<!---->
<div class="header-top">
    <div class="logo-in">
        <a href="/"><img src="{{ asset('images/logo.png')}}" alt=""></a>
    </div>
    <div class="top-nav-in">
        <span class="menu"><img src="{{ asset('images/menu.png')}}" alt=""> </span>
        <ul >
            <li @yield('inicio_active') ><a href="/" >Jey Aderith</a></li>
            <li @yield('chamba_active')><a href="{{route('chamba')}}" class="black" >La Chamba</a></li>
            <li @yield('que_active')><a href="{{route('que')}}" class="black1">¿Qué o qué?</a></li>
            <li @yield('blog_active')><a href="{{route('blog')}}" class="black2" >Comedy Blog</a></li>
            <li @yield('todo_active')><a href="{{route('todo')}}" class="black3" >Todo se puede</a></li>
            <li @yield('contacto_active')><a href="{{route('contacto')}}" class="black4" >Escríbeme</a></li>
        </ul>
        <script>
            $("span.menu").click(function(){
                $(".top-nav-in ul").slideToggle(500, function(){
                });
            });
        </script>

    </div>
    <div class="clear"> </div>
</div>
<!---->