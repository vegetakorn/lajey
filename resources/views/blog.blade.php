@section('title_prefix')
    SoyLaJey::Comedy Blog
@endsection
@section('blog_active')
    class="active"
@endsection
@extends('welcome')
@section('body')

    <div class="content" style="background-color: #fff;height: 100%" >
        <div class="blog">
            <!--for-->
            @foreach($blogs as $blog)
            <div class="blog-top">
                <div class="col-d">
                    <div class="slider1">
                        <div class="callbacks_container">
                            <ul class="rslides" id="slider1">
                                <li>
                                    <img src="images/{{$blog->imagen}}" alt="">
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="blog-in">
                        <h5><a href="{{route('blog.contenido', $blog->id)}}">{{$blog->titulo}}</a></h5>
                        <p>{{$blog->corta}} </p>
                        <ul class="date">
                            <li><span><a href="#"><i ></i>{{$blog->coments}} Comentarios</a></span></li>
                            <li><span><i class="date-in"></i>{{$blog->fecha}}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            @endforeach
            <!---->

        </div>

    </div>
    <div class="clear"> </div>

@endsection