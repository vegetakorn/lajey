@section('title_prefix')
    SoyLaJey::Blog Admin
@endsection
@section('f_blog_active')
    class="active"
@endsection
@extends('welcome')
@section('body')

    <div class="content" style="padding-left: 100px">


            <div class="top-contact">
                <h3>Blog nuevo (click a imagen para elegir)</h3>
                <input type="hidden" id="foto_blog" value="pi7.jpg" >
                <form action="{{route('regFotoBlog')}}" method="post" style="display: none" id="avatarForm">
                    <input type="file" id="avatarInput" name="photo">
                </form>
                <div class="rutaFoto">
                    <img onclick="setFoto()"  id="avatarImage" src="{{ asset('images/pi7.jpg')}}" alt="User profile picture">

                </div>
                <div class="grid-contact">
                    <div class="your-top">
                        <i> </i>
                        <input type="text" id="inputTitulo" value="Titulo" onfocus="this.value='';" onblur="if (this.value == '') {this.value = 'Titulo';}">
                        <div class="clear"> </div>
                    </div>
                    <div class="your-top">
                        <i> </i>
                        <input type="text" id="inputCorta" value="Descripción corta" onfocus="this.value='';" onblur="if (this.value == '') {this.value = 'Descripcion Corta';}">
                        <div class="clear"> </div>
                    </div>
                    <div class="your-top">
                        <i> </i>
                        <input type="text" id="inputYoutube" value="URL de youtube" onfocus="this.value='';" onblur="if (this.value == '') {this.value = 'URL de youtube';}">
                        <div class="clear"> </div>
                    </div>


                </div>
                <div class="grid-contact-in">
                    <!--<textarea cols="77" id="inputLarga" rows="5" value=" " onfocus="this.value='';" onblur="if (this.value == '') {this.value = 'Contenido';}">Contenido</textarea>-->




                </div>
                <div class="clear"> </div>
            </div>
            <div class="grid-contact-in">
                <textarea id="inputLarga"  value=" " onfocus="this.value='';" onblur="if (this.value == '') {this.value = 'Contenido';}">Contenido</textarea>

                <input type="submit" onclick="sendBlog()" value="Crear">


            </div>

    </div>
@endsection
@section('js')

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="{{ asset('alert/bootbox.min.js') }}" ></script>
    <script src="{{ asset('alert/bootbox.locales.js') }}" ></script>
    <script>

        $(document).ready(function() {
            CKEDITOR.config.removePlugins = 'image';
        });

        function sendBlog()
        {
            //bootbox.alert("Enviando mensaje...");

                        bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Creando blog...</div>' })
                        var url = '{{route('insBlog')}}';
                        var token = '{{csrf_token()}}';
                        var youtube = 0;
                        if(document.getElementById('inputYoutube').value != "URL de youtube")
                        {
                            youtube = document.getElementById('inputYoutube').value;
                        }
                        $.ajax({
                            method: 'POST',
                            url: url,
                            data: {
                                titulo: document.getElementById('inputTitulo').value,
                                corta: document.getElementById('inputCorta').value,
                                larga: CKEDITOR.instances['inputLarga'].getData(),
                                youtube: youtube,
                                imagen: document.getElementById('foto_blog').value,
                                _token: token
                            }
                        })
                            .done(function(msg){
                                console.log(msg['message']);
                                var url = "";
                                url = '<?php echo e(route('blog_admin')); ?>';//'visitas/final/'+post_id;//
                                window.location.href = url;

                            });

        }

        function setFoto()
        {
            $avatarImage = $('#avatarImage');
            $avatarInput = $('#avatarInput');
            $avatarForm = $('#avatarForm');
            $rutaFoto = $('.rutaFoto');
            $avatarInput.click();
            var token = '{{csrf_token()}}';
            $avatarInput.on('change', function () {
                var formData = new FormData();
                formData.append('photo', $avatarInput[0].files[0]);
                formData.append('_token', token);


                $.ajax({
                    url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                    method: $avatarForm.attr('method'),
                    data: formData,
                    processData: false,
                    contentType: false
                }).done(function (msg) {
                    // alert(cam);
                    /*if (data.success)
                        $avatarImage.attr('src', data.path);*/
                    var URLdomain = window.location.host;
                    var pathImg = 'http://'+URLdomain+'/'+msg['path'];

                    $avatarImage.attr('src', pathImg)
                    console.log(msg['message']);


                    document.getElementById('foto_blog').value = msg['message'];


                });
            });
        }




    </script>
    <script type="text/javascript">
        CKEDITOR.replace("inputLarga");
    </script>
@endsection