@section('title_prefix')
    SoyLaJey::Gestión de blog
@endsection
@section('f_que_active')
    class="active"
@endsection
@extends('welcome')
@section('body')

    <div class="content">
        <div class="contact">

            <table class="table">
                <tr>
                    <th>Foto</th>

                </tr>

                    <tr>
                        <td>

                            <form action="{{route('regFotoQue')}}" method="post" style="display: none" id="avatarForm{{$fotos->id}}">
                                <input type="file" id="avatarInput{{$fotos->id}}" name="photo{{$fotos->id}}">
                            </form>
                            <div class="rutaFoto{{$fotos->id}}">
                                <img onclick="setFotoIni({{$fotos->id}})"  id="avatarImage{{$fotos->id}}" src="{{ asset('images/'.$fotos->foto)}}" >

                            </div>

                        </td>
                    </tr>



            </table>
        </div>

    </div>



@endsection
@section('js')

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="{{ asset('alert/bootbox.min.js') }}" ></script>
    <script src="{{ asset('alert/bootbox.locales.js') }}" ></script>
    <script>

        function setFotoIni(Id)
        {
            $avatarImage = $('#avatarImage'+Id);
            $avatarInput = $('#avatarInput'+Id);
            $avatarForm = $('#avatarForm'+Id);
            $rutaFoto = $('.rutaFoto'+Id);
            $avatarInput.click();
            var token = '{{csrf_token()}}';
            $avatarInput.on('change', function () {
                var formData = new FormData();
                formData.append('photo', $avatarInput[0].files[0]);
                formData.append('_token', token);
                formData.append('id', Id);
                $.ajax({
                    url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                    method: $avatarForm.attr('method'),
                    data: formData,
                    processData: false,
                    contentType: false
                }).done(function (msg) {
                    // alert(cam);
                    /*if (data.success)
                        $avatarImage.attr('src', data.path);*/
                    var URLdomain = window.location.host;
                    var pathImg = 'http://'+URLdomain+'/'+msg['path'];

                    $avatarImage.attr('src', pathImg)
                    console.log(msg['message']);



                });
            });
        }


    </script>
@endsection