<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {

    return view('home');
});*/

Route::get('/', 'ChambaController@home')->name('chamba');
//Route::get('/clientes', 'ClientesController@index')->name('clientes');
//Route::get('/contacto', 'ContactoController@index')->name('contacto');
Route::get('/chamba', 'ChambaController@index')->name('chamba');
Route::get('/que', 'ChambaController@que')->name('que');

Route::get('/blog', 'ChambaController@blog')->name('blog');
Route::get('/addBlog', 'ChambaController@addBlog')->name('addBlog');
Route::post('/insBlog', 'ChambaController@insBlog')->name('insBlog');
Route::post('/updBlog', 'ChambaController@updBlog')->name('updBlog');
Route::get('/blog_admin', 'ChambaController@blog_admin')->name('blog_admin');
Route::get('/blog/contenido/{id}', 'ChambaController@contenido')->name('blog.contenido')->where('id', '[0-9]+');
Route::post('/delBlog', 'ChambaController@delBlog')->name('delBlog');
Route::get('/editBlog/{id}', 'ChambaController@editBlog')->name('editBlog')->where('id', '[0-9]+');
Route::post('/regFotoBlog', 'ChambaController@regFotoBlog')->name('regFotoBlog');

Route::get('/todo', 'ChambaController@todo')->name('todo');
Route::post('/sendMail', 'ChambaController@mail')->name('sendMail');
Route::post('/sendComent', 'ChambaController@coment')->name('sendComent');
Route::get('/contacto', 'ChambaController@contacto')->name('contacto');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/edit_inicio', 'ChambaController@edit_inicio')->name('edit_inicio');
Route::post('/regFotoIni', 'ChambaController@regFotoIni')->name('regFotoIni');

Route::get('/edit_chamba', 'ChambaController@edit_chamba')->name('edit_chamba');
Route::post('/regFotoChamba', 'ChambaController@regFotoChamba')->name('regFotoChamba');

Route::get('/edit_que', 'ChambaController@edit_que')->name('edit_que');
Route::post('/regFotoQue', 'ChambaController@regFotoQue')->name('regFotoQue');

Route::get('/edit_todo', 'ChambaController@edit_todo')->name('edit_todo');
Route::post('/regFotoTodo', 'ChambaController@regFotoTodo')->name('regFotoTodo');


Route::get('/salir', 'ChambaController@getSignOut')->name('salir');


